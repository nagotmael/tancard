﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewListOfItems", menuName = "GameData/ListOfItems", order = 8)]
public class ListOfItems : ScriptableObject
{
    public List<Item> listOfItems;

    public Item GetItem(int idItem)
    {
        Item item = null;
        foreach (Item itemsToParse in this.listOfItems)
        {
            if (itemsToParse.id == idItem)
            {
                item = itemsToParse;
            }
        }
        return item;
    }
}
