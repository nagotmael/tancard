﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewItem", menuName = "GameData/Item", order = 7)]
public class Item : ScriptableObject
{
    public int id;
    public string nameKey;
    public BonesSprite bonesSprite;
    public GameObject inventorySprite;
    public ItemType itemType;
    [System.Serializable]
    public enum ItemType { Weapon, TwoHandedWeapon, Armor, Shield, Helmet, Shoes, Ring, Item }
    public Rarity rarity;
    [System.Serializable]
    public enum Rarity { normal, rare, legendary }
    public AudioClip inventorySound;
    public AudioClip useSound;
    public bool isUsableFromInventory = false;
    public int strengthBonus;
    public int dexterityBonus;
    public int intelligenceBonus;
    public int hpBonus;
    public int mpBonus;
    public int hpMaxBonus;
    public int mpMaxBonus;

}

[System.Serializable]
public class BonesSprite
{
    public GameObject body;
    public GameObject head;
    public GameObject legLeftUp;
    public GameObject legLeftDown;
    public GameObject legRightUp;
    public GameObject legRightDown;
    public GameObject frontArmUp;
    public GameObject frontArmDown;
    public GameObject backArmUp;
    public GameObject backArmDown;
}