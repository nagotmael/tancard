﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewCard", menuName = "GameData/Card", order = 9)]
public class Card : ScriptableObject
{
    public int id;
    public string nameKey;
    public string additionalDescription;
    public int actionPoint;
    public CardType cardType;
    [System.Serializable]
    public enum CardType { Attack, Defense, Move, Skill, Magic }
    public GameObject cardImagePrefab;
    public int skill;
    public Target target;
    [System.Serializable]
    public enum Target { CurrentActor, Ally, Enemy }
    public Range range;
    public enum Range { None, All, ReachableRadius, FlyingRadius, StraightLine }
    public int rangeValue;

}