﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewAlly", menuName = "GameData/Ally", order = 5)]
public class Ally : ScriptableObject
{
    public int id;
    public GameObject allyGO;
    public float offsetX;
    public float offsetY;
    public float speed;
}
