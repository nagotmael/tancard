﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewEncouter", menuName = "GameData/Encounter", order = 4)]
public class Encounter : ScriptableObject
{
    public Vector2Int heroStartPosition;
    public bool heroIsTurningBack;
    public Vector2Int ally2StartPosition;
    public bool ally2IsTurningBack;
    public Vector2Int ally3StartPosition;
    public bool ally3IsTurningBack;
    public Vector2Int ally4StartPosition;
    public bool ally4IsTurningBack;
    public Vector2Int ally5StartPosition;
    public bool ally5IsTurningBack;
    public EnemyPosition[] enemiesPosition;
}

[System.Serializable]
public class EnemyPosition
{
    public Enemy enemy;
    public Vector2Int position;
    public bool isTurningBack;
}
