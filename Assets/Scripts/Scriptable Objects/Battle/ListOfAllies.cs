﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewListOfAllies", menuName = "GameData/ListOfAllies", order = 6)]
public class ListOfAllies : ScriptableObject
{
    public List<Ally> listOfAllies;

    public Ally GetAlly(int idAlly)
    {
        Ally ally = null;
        foreach (Ally allyToParse in this.listOfAllies)
        {
            if (allyToParse.id == idAlly)
            {
                ally = allyToParse;
            }
        }
        return ally;
    }

    public List<Ally> GetAllAlliesFromInt(List<int> listAlliesInt)
    {
        List<Ally> alliesList = new List<Ally>();
        foreach (Ally allyToParse in this.listOfAllies)
        {
            if (listAlliesInt.Contains(allyToParse.id))
            {
                alliesList.Add(allyToParse);
            }
        }
        return alliesList;
    }
}
