﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewEnemy", menuName = "GameData/Enemy", order = 3)]
public class Enemy : ScriptableObject
{
    public GameObject enemyGO;
    public float offsetX;
    public float offsetY;
    public float speed;
}