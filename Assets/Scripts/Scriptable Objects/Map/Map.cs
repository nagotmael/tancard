﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewMap", menuName = "GameData/Map", order = 1)]
public class Map : ScriptableObject
{
    public GameObject mapBackground;
    public GameObject eventBackground;
    public GameObject battleBackground;

    public AudioClip bgmFileMap;
    public AudioClip bgsFileMap;
    public AudioClip bgmFileBattle;
    public AudioClip bgsFileBattle;
    public int level;
    public int sizeX;
    public int sizeY;
    public int battlefieldSizeX;
    public int battlefieldSizeY;
    public float battlefieldGridInitX;
    public float battlefieldGridInitY;
    public Obstacle[] possibleObstacles;
    [Tooltip("How often the map tiles are connected to each other (0-never connecting, 100-always connecting)")]
    public int tilesConnectionRatio;
    [Tooltip("Is it possible to have the map tiles connection crossing each other")]
    public bool allowTileConnectionCrossingEachOther;
    public Tile[] possibleTiles;
    public PossibleEncounter[] possibleEncounters;
    public int[] possibleEvents;

}

[System.Serializable]
public class Tile
{
    [Tooltip("1-Random, 2-Fight, 3-Rest, 4-Chest")]
    public EventType eventType;
    [System.Serializable]
    public enum EventType { RandomEvent, Fight, Rest, Chest }
    [Tooltip("The prefab to be displayed for this tile")]
    public GameObject tilePrefab;
    [Tooltip("Is it the default tile of the Map?")]
    public bool isDefault;
    [Tooltip("Minimum number of tile to be displayed in the Map (-1 means no Min Number)")]
    public int minNumber = -1;
    [Tooltip("Maximum number of tile to be displayed in the Map (-1 means no Max Number)")]
    public int maxNumber = -1;
    [Tooltip("Minimum X coordinate on the map the tile can be displayed (-1 means no Min X)")]
    public int minX = -1;
    [Tooltip("Maximum X coordinate on the map the tile can be displayed (-1 means no Max X)")]
    public int maxX = -1;

}

[System.Serializable]
public class Obstacle
{
    [Tooltip("The prefab containing the graphic of the obstacle")]
    public GameObject obstaclePrefab;
    [Tooltip("Used to align x of the obstacle with x of the Hexagon")]
    public float xToHex;
    [Tooltip("Used to align y of the obstacle with y of the Hexagon")]
    public float yToHex;
    [Tooltip("Minimum X to display it on the Hexa Grid")]
    public int minX;
    [Tooltip("Maximum X to display it on the Hexa Grid")]
    public int maxX;
    [Tooltip("Minimum Y to display it on the Hexa Grid")]
    public int minY;
    [Tooltip("Maximum Y to display it on the Hexa Grid")]
    public int maxY;
    [Tooltip("Probability of having the obstacle being displayed")]
    public int probalityOfDisplayed;
}

[System.Serializable]
public class PossibleEncounter
{
    public Encounter encounter;
    public int minX;
    public int maxX;
}
