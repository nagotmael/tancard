﻿using UnityEngine;

public class HeroEquipment : MonoBehaviour
{
    public GameObject heroGO;
    public static HeroEquipment instance;
    [SerializeField]
    private ListOfItems listOfItems;
    [SerializeField]
    private GameObject rightLegBoneUp;
    [SerializeField]
    private GameObject rightLegBoneDown;
    [SerializeField]
    private GameObject leftLegBoneUp;
    [SerializeField]
    private GameObject leftLegBoneDown;
    [SerializeField]
    private GameObject bodyBone;
    [SerializeField]
    private GameObject frontArmBoneUp;
    [SerializeField]
    private GameObject frontArmBoneDown;
    [SerializeField]
    private GameObject backArmBoneUp;
    [SerializeField]
    private GameObject backArmBonedown;
    [SerializeField]
    private GameObject headBone;

    void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// The sprite of the hero's equipment are displayed on the battlefield
    /// This function takes care of showing all the equipped objects on the hero sprite
    /// </summary>
    public void EquipHero()
    {
        EquipFrontHand();
        EquipBackHand();
        EquipArmor();
        EquipHelmet();
        EquipShoes();
    }

    /// <summary>
    /// This function displays the front hand item on the hero sprite if any
    /// </summary>
    private void EquipFrontHand()
    {
        if (DataController.instance.gameData.equipment.frontHandItem != 0)
        {
            BonesSprite frontHandBones = listOfItems.GetItem(DataController.instance.gameData.equipment.frontHandItem).bonesSprite;
            DisplayEquipmentSprites(frontHandBones, 1);
        }
    }

    /// <summary>
    /// This function displays the back hand item on the hero sprite if any
    /// </summary>
    private void EquipBackHand()
    {
        if (DataController.instance.gameData.equipment.backHandItem != 0)
        {
            BonesSprite backHandBones = listOfItems.GetItem(DataController.instance.gameData.equipment.backHandItem).bonesSprite;
            DisplayEquipmentSprites(backHandBones, 2);
        }
    }

    /// <summary>
    /// This function displays the armor item on the hero sprite if any
    /// </summary>
    private void EquipArmor()
    {
        if (DataController.instance.gameData.equipment.armorItem != 0)
        {
            BonesSprite armorBones = listOfItems.GetItem(DataController.instance.gameData.equipment.armorItem).bonesSprite;
            DisplayEquipmentSprites(armorBones);
        }
    }

    /// <summary>
    /// This function displays the helmet item on the hero sprite if any
    /// </summary>
    private void EquipHelmet()
    {
        if (DataController.instance.gameData.equipment.helmetItem != 0)
        {
            BonesSprite helmetBones = listOfItems.GetItem(DataController.instance.gameData.equipment.helmetItem).bonesSprite;
            DisplayEquipmentSprites(helmetBones);
        }
    }

    /// <summary>
    /// This function displays the shoes item on the hero sprite if any
    /// </summary>
    private void EquipShoes()
    {
        if (DataController.instance.gameData.equipment.shoesItem != 0)
        {
            BonesSprite shoesBones = listOfItems.GetItem(DataController.instance.gameData.equipment.shoesItem).bonesSprite;
            DisplayEquipmentSprites(shoesBones);
        }
    }

    /// <summary>
    /// This function displays an item sprite on its correct bone
    /// An item can have several sprites, for example a shoe item has to be displayed on 2 different bones
    /// </summary>
    /// <param name="bonesSprites">The set of sprites to be displayed on each bone of the hero</param>
    /// <param name="WeaponFrontArmOrBackArm">0 if the item is equipped on the front hand, 1 if equipped on the back hand</param>
    private void DisplayEquipmentSprites(BonesSprite bonesSprites, int WeaponFrontArmOrBackArm = 0)
    {
        if (bonesSprites.legRightDown != null)
        {
            GameObject.Instantiate(bonesSprites.legRightDown, rightLegBoneDown.transform);
        }
        if (bonesSprites.legRightUp != null)
        {
            GameObject.Instantiate(bonesSprites.legRightUp, rightLegBoneUp.transform);
        }
        if (bonesSprites.legLeftDown != null)
        {
            GameObject.Instantiate(bonesSprites.legLeftDown, leftLegBoneDown.transform);
        }
        if (bonesSprites.legLeftUp != null)
        {
            GameObject.Instantiate(bonesSprites.legLeftUp, leftLegBoneUp.transform);
        }
        if (bonesSprites.body != null)
        {
            GameObject.Instantiate(bonesSprites.body, bodyBone.transform);
        }
        if (bonesSprites.head != null)
        {
            GameObject.Instantiate(bonesSprites.head, headBone.transform);
        }
        if (bonesSprites.backArmDown != null && WeaponFrontArmOrBackArm != 1)
        {
            GameObject.Instantiate(bonesSprites.backArmDown, backArmBonedown.transform);
        }
        if (bonesSprites.backArmUp != null)
        {
            GameObject.Instantiate(bonesSprites.backArmUp, backArmBoneUp.transform);
        }
        if (bonesSprites.frontArmUp != null)
        {
            GameObject.Instantiate(bonesSprites.frontArmUp, frontArmBoneUp.transform);
        }
        if (bonesSprites.frontArmDown != null && WeaponFrontArmOrBackArm != 2)
        {
            GameObject.Instantiate(bonesSprites.frontArmDown, frontArmBoneDown.transform);
        }
    }
}