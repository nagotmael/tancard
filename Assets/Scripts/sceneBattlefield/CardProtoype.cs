﻿using UnityEngine;

public class CardProtoype : MonoBehaviour
{
    public GameObject card1;
    public GameObject card2;
    public GameObject card3;
    public GameObject card4;
    public GameObject card5;
    public GameObject card6;
    public GameObject card7;
    public GameObject card8;
    public GameObject card9;

    // Start is called before the first frame update
    void Start()
    {
        card1.SetActive(true);
        card2.SetActive(true);
        card3.SetActive(true);
        card4.SetActive(true);
        card5.SetActive(true);
        card6.SetActive(true);
        card7.SetActive(true);
        card8.SetActive(true);
        card9.SetActive(true);
    }
}
