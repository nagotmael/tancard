﻿using UnityEngine;

public class HexInteractions : MonoBehaviour
{
    public bool highlighted = false;
    public bool selected = false;
    private int xHex;
    private int yHex;

    public void SetCoordinates(int x, int y)
    {
        xHex = x;
        yHex = y;
    }
    public void Highlight()
    {
        Animator anim = this.transform.GetChild(0).gameObject.GetComponent<Animator>();
        anim.SetTrigger("Highlighted");
        highlighted = true;
    }

    public void RemoveHighlight()
    {
        if (highlighted)
        {
            Animator anim = this.transform.GetChild(0).gameObject.GetComponent<Animator>();
            anim.SetTrigger("Highlighted");
        }
        highlighted = false;
    }

    public void Select()
    {
        Animator anim = this.transform.GetChild(0).gameObject.GetComponent<Animator>();
        anim.SetTrigger("Selected");
        selected = true;
    }

    public void RemoveSelection()
    {
        if (selected)
        {
            Animator anim = this.transform.GetChild(0).gameObject.GetComponent<Animator>();
            anim.SetTrigger("Selected");
        }
        selected = false;
    }

    public void OnClick()
    {
        if (highlighted)
        {
            BattleState.instance.hex = new Vector2Int(xHex, yHex);
            BattleState.instance.SetCurrentStateTo("HexSelected");
        }
    }
}