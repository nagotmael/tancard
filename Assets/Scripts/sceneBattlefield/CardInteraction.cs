﻿using Lean.Touch;
using UnityEngine;

public class CardInteraction : MonoBehaviour
{
    public Vector3 initialPosition;
    public Quaternion initialRotation;
    public int initialSortingOrder;
    public Vector3 initialScale = new Vector3(0.65f, 0.65f, 1);
    public Vector3 zoomedScale = new Vector3(0.9f, 0.9f, 1);
    public float zoomedY = 0;
    private bool isLongTapped = false;
    private GameObject leanFingerLine;

    void Start()
    {
        initialPosition = this.gameObject.transform.parent.transform.position;
        initialRotation = this.gameObject.transform.parent.transform.rotation;
        initialSortingOrder = this.gameObject.GetComponent<SpriteRenderer>().sortingOrder;
        leanFingerLine = GameObject.Find("LeanFingerLineFade");
    }

    /// <summary>
    /// This method is called when tapping on a card
    /// </summary>
    public void OnCardTap()
    {
        StartCoroutine("PlayClickSound");
    }

    /// <summary>
    /// Called when long tapping a card
    /// This shows it bigger and upper
    /// </summary>
    public void OnCardLongTapDown()
    {
        leanFingerLine.GetComponent<LeanFingerLineFade>().Target = this.gameObject.transform;
        isLongTapped = true;
        Debug.Log("Long Tapped on " + this.gameObject.transform.parent.name);
        this.gameObject.transform.parent.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        this.gameObject.transform.parent.transform.localScale = zoomedScale;
        StartCoroutine(GridController.instance.MoveFromTo(this.gameObject.transform.parent, this.gameObject.transform.parent.transform.position, new Vector2(this.gameObject.transform.parent.transform.position.x, zoomedY), 55));
        // this.gameObject.transform.parent.transform.localPosition = new Vector2(this.gameObject.transform.parent.transform.localPosition.x, -3.56f);
        GridController.instance.SetGameObjectSortingOrder(this.gameObject.transform.parent.gameObject, 0);
    }

    /// <summary>
    /// Called after stopping long tapping a card.
    /// Move it back to its initial position.
    /// </summary>
    public void OnCardLongTapUp()
    {
        // Function call to not display the line before the finger moves
        if (GridController.instance.lineOrientation == "up")
        {
            EnableLineRenderer();
        }
        isLongTapped = false;
        MoveCardToInitialPosition();
    }

    /// <summary>
    /// Then the player long tap a card and then moves his finger, a line is displayed between the card and the finger
    /// This functions shows the line
    /// </summary>
    private void EnableLineRenderer()
    {
        GameObject[] lines = GameObject.FindGameObjectsWithTag("Line");
        foreach (GameObject line in lines)
        {
            line.GetComponent<LineRenderer>().enabled = true;
        }
    }

    /// <summary>
    /// When the player long tap on a card, it is shown upper and bigger.
    /// When the player releases its finger, the card goes back to its initial position
    /// This function takes care of moving the card to its initial position
    /// </summary>
    private void MoveCardToInitialPosition()
    {
        StartCoroutine(GridController.instance.MoveFromTo(this.gameObject.transform.parent, this.gameObject.transform.parent.transform.position, new Vector2(initialPosition.x, initialPosition.y), 55));

        this.gameObject.transform.parent.transform.rotation = initialRotation;
        this.gameObject.transform.parent.transform.localScale = initialScale;
    }

    void Update()
    {
        if (this.gameObject.transform.parent.transform.position.y == zoomedY && this.gameObject.transform.parent.transform.localScale == initialScale)
        {
            MoveCardToInitialPosition();
        }
        // if no line active and sorting order is not the initial one, then replace the card on its initial order
        if (GridController.instance.fingerPositionWhenDrawingLine.x == -9999 && this.gameObject.GetComponent<SpriteRenderer>().sortingOrder != initialSortingOrder)
        {
            GridController.instance.SetGameObjectSortingOrder(this.gameObject.transform.parent.gameObject, 50 - initialSortingOrder);
        }
    }

    private void PlayClickSound()
    {
        StartCoroutine(SoundController.instance.PlaySE(SoundContainerMap.instance.clickSound, 1));
    }
}