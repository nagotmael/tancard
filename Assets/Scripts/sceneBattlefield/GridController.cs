﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Anima2D;
using UnityEngine;

public class GridController : MonoBehaviour
{
    public static GridController instance;
    //following public variable is used to store the hex model prefab;
    //instantiate it by dragging the prefab on this variable using unity editor
    public GameObject hex;
    //next two variables can also be instantiated using unity editor
    private int gridWidthInHexes;
    private int gridHeightInHexes;
    public ListOfMaps listOfMaps;

    //Hexagon tile width and height in game world
    private float hexWidth;
    private float hexHeight;
    public Map map;
    public GameObject rootObject;
    public List<ObstacleListItem> obstaclesList;
    public GameObject playerGO;
    public ActorPosition playerPosition;
    public List<ActorPosition> enemiesPosition = new List<ActorPosition>();
    public List<ActorPosition> alliesPosition = new List<ActorPosition>();
    private List<Vector2Int> hexagonsList = new List<Vector2Int>();
    private List<GameObject> hexagonsObjectList = new List<GameObject>();
    public Encounter encounter;
    public List<Ally> allies;

    // variables used for the line drawn when moving a card
    public Vector2 fingerPositionWhenDrawingLine;
    public string lineOrientation = "";

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // void Update () {
    // if (GetHexUnderDrawingLine(fingerPositionWhenDrawingLine) != null) { getHexUnderDrawingLine(fingerPositionWhenDrawingLine).GetComponent<HexInteractions>().select(); }
    // }

    /// <summary>
    /// This function initialized the Hexagon width and height
    /// </summary>
    private void SetSizes()
    {
        gridWidthInHexes = map.battlefieldSizeX;
        gridHeightInHexes = map.battlefieldSizeY;
        //renderer component attached to the Hex prefab is used to get the current width and height
        hexWidth = hex.GetComponent<Renderer>().bounds.size.x;
        hexHeight = hex.GetComponent<Renderer>().bounds.size.y;
    }

    /// <summary>
    /// This function calculates the position of the first hexagon tile
    /// </summary>
    private Vector3 CalcInitPos()
    {
        Vector3 initPos;
        //the initial position will be in the left upper corner
        initPos = new Vector3(map.battlefieldGridInitX, map.battlefieldGridInitY, 0);
        return initPos;
    }

    /// <summary>
    /// This function converts the hex grid coordinates to game world coordinates
    /// </summary>
    /// <param name="gridPos">The hex grid coordinate</param>
    public Vector3 CalcWorldCoord(Vector2 gridPos)
    {
        //Position of the first hex tile
        Vector3 initPos = CalcInitPos();
        //Every second row is offset by half of the tile width
        float offset = 0;
        if (gridPos.y % 2 != 0)
            offset = hexWidth / 2;

        float x = initPos.x + offset + gridPos.x * (hexWidth + 0.02f);
        //Every new line is offset in z direction by 3/4 of the hexagon height
        float y = initPos.y - gridPos.y * (hexHeight + 0.02f) * 0.75f;
        return new Vector3(x, y);
    }

    /// <summary>
    /// This function initializes and position all the tiles
    /// </summary>
    public void CreateGrid()
    {
        SetSizes();
        //Game object which is the parent of all the hex tiles
        GameObject hexGridGO = new GameObject("HexGrid");
        hexGridGO.transform.SetParent(rootObject.transform);

        for (float y = 0; y < gridHeightInHexes; y++)
        {
            for (float x = 0; x < gridWidthInHexes; x++)
            {
                //GameObject assigned to Hex public variable is cloned
                GameObject hexGO = (GameObject)Instantiate(hex);
                int hexX = Mathf.FloorToInt(x) + 1 + Mathf.FloorToInt((y + 1) / 2);
                int hexY = gridHeightInHexes - Mathf.FloorToInt(y);
                hexagonsList.Add(new Vector2Int(hexX, hexY));
                hexagonsObjectList.Add(hexGO);
                hexGO.name = "Hex " + hexX + "-" + hexY;
                hexGO.GetComponent<HexInteractions>().SetCoordinates(hexX, hexY);
                //Current position in grid
                Vector2 gridPos = new Vector2(x, y);
                hexGO.transform.position = CalcWorldCoord(gridPos);
                hexGO.transform.SetParent(hexGridGO.transform);
            }
        }
    }

    /// <summary>
    /// Return the hexagon Game Object being at the position x and y on the grid
    /// </summary>
    /// <param name="x">The x position of hex on the grid</param>
    /// <param name="y">The y position of hex on the grid</param>
    public GameObject GetHexagon(int x, int y)
    {
        return GameObject.Find("Hex " + x + "-" + y);
    }

    /// <summary>
    /// Return the grid coordinates of an hexagon from its gameobject
    /// </summary>
    /// <param name="hexGO">The GameObject to return the grid coordinates from</param>
    public Vector2Int GetHexagonCoordinates(GameObject hexGO)
    {
        string hexName = hexGO.name;
        string coordinatesText = hexName.Split(" "[0])[1];
        return new Vector2Int(int.Parse(coordinatesText.Split("-"[0])[0]), int.Parse(coordinatesText.Split("-"[0])[1]));
    }

    /// <summary>
    /// This function generates the obstacles that will be shown on the grid but do not display them
    /// The obstacles generated depends on how the map's scriptable object is configured, this function takes that into account
    /// </summary>
    public void GenerateObstacles()
    {
        obstaclesList = new List<ObstacleListItem>();
        for (int i = 0; i < map.possibleObstacles.Length; i++)
        {
            if (UnityEngine.Random.Range(0, 100) < map.possibleObstacles[i].probalityOfDisplayed)
            {
                int j = 0;
                // Prevent infinite loop
                while (j < 1000)
                {
                    int xObstacle = UnityEngine.Random.Range(map.possibleObstacles[i].minX, map.possibleObstacles[i].maxX + 1);
                    int yObstacle = UnityEngine.Random.Range(map.possibleObstacles[i].minY, map.possibleObstacles[i].maxY + 1);
                    if (!CheckIfObstacleAtPosition(xObstacle, yObstacle))
                    {
                        obstaclesList.Add(new ObstacleListItem(new Vector2Int(xObstacle, yObstacle), i));
                        break;
                    }
                    j++;
                }
            }
        }
    }

    /// <summary>
    /// Check if an obstacle exists at the grid position in parameter
    /// </summary>
    /// <param name="x">The x position of hex on the grid</param>
    /// <param name="y">The y position of hex on the grid</param>
    public bool CheckIfObstacleAtPosition(int x, int y)
    {
        for (int i = 0; i < obstaclesList.Count; i++)
        {
            if ((obstaclesList[i].obstaclePosition.x == x) && (obstaclesList[i].obstaclePosition.y == y))
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// This function takes care of displaying the obstacles on the grid once they have been generated
    /// </summary>
    public void DisplayObstacles()
    {
        for (int i = 0; i < obstaclesList.Count; i++)
        {
            GameObject obstacleGO = (GameObject)Instantiate(map.possibleObstacles[obstaclesList[i].obstacleId].obstaclePrefab);
            obstacleGO.transform.position = GetHexagon(obstaclesList[i].obstaclePosition.x, obstaclesList[i].obstaclePosition.y).transform.position;
            obstacleGO.transform.position = new Vector2(obstacleGO.transform.position.x + map.possibleObstacles[obstaclesList[i].obstacleId].xToHex, obstacleGO.transform.position.y + map.possibleObstacles[obstaclesList[i].obstacleId].yToHex);
            SetGameObjectSortingOrder(obstacleGO, obstaclesList[i].obstaclePosition.y);
        }
    }

    /// <summary>
    /// Since the objects are moving on the grid, this function takes care of changing the display order so object on the front
    /// are displayed in front of object in the back
    /// It also spread the sorting order to the children of the object in parameter
    /// </summary>
    /// <param name="go">The game object we want to change the sorting order</param>
    /// <param name="order">The sorting order</param>
    public void SetGameObjectSortingOrder(GameObject go, int order)
    {
        SpriteRenderer sprite = go.GetComponent<SpriteRenderer>();
        if (sprite)
        {
            sprite.sortingOrder = 50 - order;
        }
        // Handle the hero case that has spriteMesh instead of sprite
        SpriteMeshInstance spriteMesh = go.GetComponent<SpriteMeshInstance>();
        if (spriteMesh)
        {
            spriteMesh.sortingOrder = 50 - order;
        }

        Canvas canvasComponent = go.GetComponent<Canvas>();
        if (canvasComponent)
        {
            canvasComponent.sortingOrder = 50 - order;
        }

        // Iterate to apply the Sorting Order to children as well
        foreach (Transform child in go.transform)
        {
            SetGameObjectSortingOrder(child.gameObject, order);
        }
    }

    /// <summary>
    /// Initialize the hero, allies and enemies position on the grid depending on how the encounter scriptable object is configured
    /// </summary>
    public void InitializeActorsPosition()
    {
        initializeHeroPosition();
        initializeAlliesPosition();
        initializeEnemiesPosition();
    }

    /// <summary>
    /// Initialize the hero position on the grid depending on how the encounter scriptable object is configured
    /// </summary>
    private void initializeHeroPosition()
    {
        Vector2Int heroPosition = encounter.heroStartPosition;
        Vector2 heroOffset = new Vector2(-0.03f, 0.51f);
        float heroSpeed = 1.6f;
        SetGameObjectSortingOrder(playerGO, heroPosition.y);
        Transform initHex = GetHexagon(heroPosition.x, heroPosition.y).transform;
        if (encounter.heroIsTurningBack)
        {
            playerGO.transform.localScale = new Vector3(-playerGO.transform.localScale.x, playerGO.transform.localScale.y, playerGO.transform.localScale.z);
            playerGO.transform.position = new Vector2(initHex.position.x - heroOffset.x, initHex.position.y + heroOffset.y);
        }
        else
        {
            playerGO.transform.position = new Vector2(initHex.position.x + heroOffset.x, initHex.position.y + heroOffset.y);
        }
        playerPosition = new ActorPosition(playerGO, heroPosition, encounter.heroIsTurningBack, heroOffset, heroSpeed);
    }

    /// <summary>
    /// Initialize the allies position on the grid depending on how the encounter scriptable object is configured
    /// </summary>
    private void initializeAlliesPosition()
    {
        for (int i = 0; i < allies.Count; i++)
        {
            GameObject allyGO = (GameObject)Instantiate(allies[i].allyGO);
            Vector2Int allyPosition = new Vector2Int();
            switch (i)
            {
                case 0:
                    allyPosition = encounter.ally2StartPosition;
                    break;
                case 1:
                    allyPosition = encounter.ally3StartPosition;
                    break;
                case 2:
                    allyPosition = encounter.ally4StartPosition;
                    break;
                case 3:
                    allyPosition = encounter.ally5StartPosition;
                    break;
                default:
                    break;
            }
            SetGameObjectSortingOrder(allyGO, allyPosition.y);
            Transform initAllyHex = GetHexagon(allyPosition.x, allyPosition.y).transform;
            bool isAllyTurningBack = false;
            switch (i)
            {
                case 0:
                    isAllyTurningBack = encounter.ally2IsTurningBack;
                    break;
                case 1:
                    isAllyTurningBack = encounter.ally3IsTurningBack;
                    break;
                case 2:
                    isAllyTurningBack = encounter.ally4IsTurningBack;
                    break;
                case 3:
                    isAllyTurningBack = encounter.ally5IsTurningBack;
                    break;
                default:
                    break;
            }
            if (isAllyTurningBack)
            {
                allyGO.transform.localScale = new Vector3(-allyGO.transform.localScale.x, allyGO.transform.localScale.y, allyGO.transform.localScale.z);
                allyGO.transform.position = new Vector2(initAllyHex.position.x - allies[i].offsetX, initAllyHex.position.y + allies[i].offsetY);
            }
            else
            {
                allyGO.transform.position = new Vector2(initAllyHex.position.x + allies[i].offsetX, initAllyHex.position.y + allies[i].offsetY);
            }
            ActorPosition ap = new ActorPosition(allyGO, allyPosition, isAllyTurningBack, new Vector2(allies[i].offsetX, allies[i].offsetY), allies[i].speed);
            alliesPosition.Add(ap);
        }
    }

    /// <summary>
    /// Initialize the enemies position on the grid depending on how the encounter scriptable object is configured
    /// </summary>
    private void initializeEnemiesPosition()
    {
        for (int i = 0; i < encounter.enemiesPosition.Length; i++)
        {
            GameObject enemyGO = (GameObject)Instantiate(encounter.enemiesPosition[i].enemy.enemyGO);
            Vector2Int enemyPosition = encounter.enemiesPosition[i].position;
            SetGameObjectSortingOrder(enemyGO, enemyPosition.y);
            Transform initEnemyHex = GetHexagon(enemyPosition.x, enemyPosition.y).transform;
            if (encounter.enemiesPosition[i].isTurningBack)
            {
                enemyGO.transform.localScale = new Vector3(-enemyGO.transform.localScale.x, enemyGO.transform.localScale.y, enemyGO.transform.localScale.z);
                enemyGO.transform.position = new Vector2(initEnemyHex.position.x - encounter.enemiesPosition[i].enemy.offsetX, initEnemyHex.position.y + encounter.enemiesPosition[i].enemy.offsetY);
            }
            else
            {
                enemyGO.transform.position = new Vector2(initEnemyHex.position.x + encounter.enemiesPosition[i].enemy.offsetX, initEnemyHex.position.y + encounter.enemiesPosition[i].enemy.offsetY);
            }
            ActorPosition ap = new ActorPosition(enemyGO, enemyPosition, encounter.enemiesPosition[i].isTurningBack, new Vector2(encounter.enemiesPosition[i].enemy.offsetX, encounter.enemiesPosition[i].enemy.offsetY), encounter.enemiesPosition[i].enemy.speed);
            enemiesPosition.Add(ap);
        }
    }

    /// <summary>
    /// Activate the highlight animation on the tile (hex) in parameter
    /// </summary>
    /// <param name="hexPosition">The position of the tile (hex) to highlight</param>
    public void HighlightTile(Vector2Int hexPosition)
    {
        GameObject hexagonGO = GetHexagon(hexPosition.x, hexPosition.y);
        if (hexagonGO != null)
        {
            hexagonGO.GetComponent<HexInteractions>().Highlight();
        }
    }

    /// <summary>
    /// Activate the select animation on the tile (hex) in parameter
    /// </summary>
    /// <param name="hexPosition">The position of the tile (hex) to select</param>

    public void SelectTile(Vector2Int hexPosition)
    {
        GameObject hexagonGO = GetHexagon(hexPosition.x, hexPosition.y);
        if (hexagonGO != null)
        {
            hexagonGO.GetComponent<HexInteractions>().Select();
        }
    }

    /// <summary>
    /// Deactivate the highlight animation on all the tiles (hex)
    /// </summary>
    public void RemoveHightlightAllTiles()
    {
        for (int i = 0; i < hexagonsList.Count; i++)
        {
            GetHexagon(hexagonsList[i].x, hexagonsList[i].y).GetComponent<HexInteractions>().RemoveHighlight();
        }
    }

    /// <summary>
    /// Remove the select animation on the all the tiles (hex)
    /// </summary>
    public void RemoveSelectAllTiles()
    {
        for (int i = 0; i < hexagonsList.Count; i++)
        {
            GetHexagon(hexagonsList[i].x, hexagonsList[i].y).GetComponent<HexInteractions>().RemoveSelection();
        }
    }

    /// <summary>
    /// This function returns all the hex next to the one in parameters, that have no actor or no obstacle on it
    /// </summary>
    /// <param name="hexPosition">The position of the hexagon we want to return the empty neighbours</param>
    public List<Vector2Int> GetEmptyNeighbours(Vector2Int hexPosition)
    {
        GameObject hexagonGO;
        List<Vector2Int> positionsList = new List<Vector2Int>();
        hexagonGO = GetHexagon(hexPosition.x - 1, hexPosition.y);
        if (hexagonGO != null && !CheckIfObstacleAtPosition(hexPosition.x - 1, hexPosition.y) && !CheckIfActorAtPosition(hexPosition.x - 1, hexPosition.y))
        {
            positionsList.Add(new Vector2Int(hexPosition.x - 1, hexPosition.y));
        }
        hexagonGO = GetHexagon(hexPosition.x - 1, hexPosition.y + 1);
        if (hexagonGO != null && !CheckIfObstacleAtPosition(hexPosition.x - 1, hexPosition.y + 1) && !CheckIfActorAtPosition(hexPosition.x - 1, hexPosition.y + 1))
        {
            positionsList.Add(new Vector2Int(hexPosition.x - 1, hexPosition.y + 1));
        }
        hexagonGO = GetHexagon(hexPosition.x, hexPosition.y + 1);
        if (hexagonGO != null && !CheckIfObstacleAtPosition(hexPosition.x, hexPosition.y + 1) && !CheckIfActorAtPosition(hexPosition.x, hexPosition.y + 1))
        {
            positionsList.Add(new Vector2Int(hexPosition.x, hexPosition.y + 1));
        }
        hexagonGO = GetHexagon(hexPosition.x + 1, hexPosition.y);
        if (hexagonGO != null && !CheckIfObstacleAtPosition(hexPosition.x + 1, hexPosition.y) && !CheckIfActorAtPosition(hexPosition.x + 1, hexPosition.y))
        {
            positionsList.Add(new Vector2Int(hexPosition.x + 1, hexPosition.y));
        }
        hexagonGO = GetHexagon(hexPosition.x, hexPosition.y - 1);
        if (hexagonGO != null && !CheckIfObstacleAtPosition(hexPosition.x, hexPosition.y - 1) && !CheckIfActorAtPosition(hexPosition.x, hexPosition.y - 1))
        {
            positionsList.Add(new Vector2Int(hexPosition.x, hexPosition.y - 1));
        }
        hexagonGO = GetHexagon(hexPosition.x + 1, hexPosition.y - 1);
        if (hexagonGO != null && !CheckIfObstacleAtPosition(hexPosition.x + 1, hexPosition.y - 1) && !CheckIfActorAtPosition(hexPosition.x + 1, hexPosition.y - 1))
        {
            positionsList.Add(new Vector2Int(hexPosition.x + 1, hexPosition.y - 1));
        }
        return positionsList;
    }

    /// <summary>
    /// This function checks if there is an actor (hero, enemy or ally) at the hexagon having the position in parameters
    /// </summary>
    /// <param name="x">The x position of the hexagon we want to check if any actor on it</param>
    /// <param name="y">The y position of the hexagon we want to check if any actor on it</param>
    private bool CheckIfActorAtPosition(int x, int y)
    {
        if (playerPosition == null)
        {
            return false;
        }

        if (playerPosition.position.x == x && playerPosition.position.y == y)
        {
            return true;
        }

        for (int i = 0; i < enemiesPosition.Count; i++)
        {
            if (enemiesPosition[i].position.x == x && enemiesPosition[i].position.y == y)
            {
                return true;
            }
        }
        for (int i = 0; i < alliesPosition.Count; i++)
        {
            if (alliesPosition[i].position.x == x && alliesPosition[i].position.y == y)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// This function returns all the hex being in the radius of the hexagon in parameter, that have no actor or no obstacle on it
    /// </summary>
    /// <param name="hexPosition">The position of the hexagon we want to return the empty neighbours in radius</param>
    /// <param name="radius">The maximum distance from the hexagon, we want the empty neighbours to be</param>
    public List<Vector2Int> GetEmptyHexInRadius(Vector2Int hexPosition, int radius)
    {
        List<Vector2Int> positionsList = new List<Vector2Int>();
        positionsList = GetEmptyNeighbours(hexPosition);
        // Get the neighbours recursively
        for (int i = 0; i < radius - 1; i++)
        {
            int count = positionsList.Count;
            for (int j = 0; j < count; j++)
            {
                List<Vector2Int> listItemNeighbours = GetEmptyNeighbours(positionsList[j]);
                positionsList = positionsList.Concat(listItemNeighbours).ToList();
                // Eliminate duplicate in the list
                positionsList = positionsList.Distinct().ToList();
            }
        }
        // Remove the hexPosition from the list
        for (int i = 0; i < positionsList.Count; i++)
        {
            if (positionsList[i].x == hexPosition.x && positionsList[i].y == hexPosition.y)
            {
                positionsList.RemoveAt(i);
                break;
            }
        }
        return positionsList;
    }

    /// <summary>
    /// This function highlights all the hex being in the radius of the hexagon in parameter, that have no actor or no obstacle on it
    /// </summary>
    /// <param name="hexPosition">The position of the hexagon we want to highlight the empty neighbours in radius</param>
    /// <param name="radius">The maximum distance from the hexagon, we want the empty neighbours to be</param>
    public void HighlightEmptyHexInRadius(Vector2Int hexPosition, int radius)
    {
        List<Vector2Int> hexToHighlight = GetEmptyHexInRadius(hexPosition, radius);
        for (int i = 0; i < hexToHighlight.Count; i++)
        {
            HighlightTile(hexToHighlight[i]);
        }
    }

    /// <summary>
    /// This function returns one the possible shortest path between the 2 hexagons in parameters. It uses a A* pathfinding system (Self written from scratch, hence can be optimized a lot)
    /// </summary>
    /// <param name="origin">The position of the hexagon we want to calculate the path from</param>
    /// <param name="destination">The position of the hexagon we want to calculate the path to</param>
    public List<HexPath> GetHexPath(Vector2Int origin, Vector2Int destination)
    {
        List<HexPath> hexPaths = new List<HexPath>();
        List<Vector2Int> positionsList = new List<Vector2Int>();
        List<HexPath> resultPath;
        positionsList = GetEmptyNeighbours(origin);
        int count = positionsList.Count;
        for (int j = 0; j < count; j++)
        {
            // If the destination is a direct neighbour return origin and destination as path and don't iterate
            if (positionsList[j].x == destination.x && positionsList[j].y == destination.y)
            {
                resultPath = new List<HexPath>();
                resultPath.Add(new HexPath(origin, positionsList[j]));
                return resultPath;
            }
            // Add the neighbours to all the temporary paths stored
            hexPaths.Add(new HexPath(origin, positionsList[j]));
        }
        bool destReached = false;
        // Iterate through the neighbour until we find the destination
        while (true)
        {
            // Iterate to get the neighbours of the destination path
            count = hexPaths.Count;
            for (int i = 0; i < count; i++)
            {
                positionsList = GetEmptyNeighbours(hexPaths[i].destPath);
                for (int j = 0; j < positionsList.Count; j++)
                {
                    // Additional loop to check if the neighbour is already in the temporary paths
                    // If yes then it should not be added again
                    bool isItemInPath = false;
                    for (int k = 0; k < hexPaths.Count; k++)
                    {
                        if (hexPaths[k].IsDestinationPathEqualTo(positionsList[j]))
                        {
                            isItemInPath = true;
                            break;
                        }
                    }
                    if (!isItemInPath)
                    {
                        hexPaths.Add(new HexPath(hexPaths[i].destPath, positionsList[j]));
                    }
                    // Get out of the loop if desination found among the neighbours
                    if (positionsList[j].x == destination.x && positionsList[j].y == destination.y)
                    {
                        destReached = true;
                        break;
                    }
                }
            }
            if (destReached) { break; }
        }
        resultPath = new List<HexPath>();
        Vector2Int currentDest = destination;
        // It goes through all the paths stored to get a path from the origin to the destination
        while (true)
        {
            for (int i = 0; i < hexPaths.Count; i++)
            {
                if (hexPaths[i].IsDestinationPathEqualTo(currentDest))
                {
                    resultPath.Insert(0, hexPaths[i]);
                    // Debug.Log("[" + hexPaths[i].originPath.x + "," + hexPaths[i].originPath.y + "-" + hexPaths[i].destPath.x + "," + hexPaths[i].destPath.y + "]");
                    currentDest = hexPaths[i].originPath;
                    break;
                }
            }
            if (currentDest.x == origin.x && currentDest.y == origin.y)
            {
                break;
            }
        }
        return resultPath;
    }

    /// <summary>
    /// This function moves the actor in parameter to the destination in parameter using the A* pathfinding.
    /// It also takes care of mirroring the actor vertically when it goes backward
    /// </summary>
    /// <param name="actorPosition">The class storing the actor position and object</param>
    /// <param name="dest">The position of the hexagon we want to move the actor to</param>
    public IEnumerator MoveCharacterTo(ActorPosition actorPosition, Vector2Int dest)
    {
        Animator anim = actorPosition.actorGO.transform.GetChild(0).gameObject.GetComponent<Animator>();
        anim.SetTrigger("Moving");
        List<HexPath> path = GetHexPath(actorPosition.position, dest);
        for (int i = 0; i < path.Count; i++)
        {
            Vector2 tmpDestination = GetHexagon(path[i].destPath.x, path[i].destPath.y).transform.position;
            if (actorPosition.actorGO.transform.position.x > tmpDestination.x)
            {
                if (!actorPosition.isTurningBack)
                {
                    actorPosition.actorGO.transform.position = new Vector3(actorPosition.actorGO.transform.position.x - actorPosition.offset.x * 2, actorPosition.actorGO.transform.position.y, actorPosition.actorGO.transform.position.z);
                }
                actorPosition.actorGO.transform.localScale = new Vector3(-Mathf.Abs(actorPosition.actorGO.transform.localScale.x), actorPosition.actorGO.transform.localScale.y, actorPosition.actorGO.transform.localScale.z);
                actorPosition.isTurningBack = true;
            }
            else
            {
                if (actorPosition.isTurningBack)
                {
                    actorPosition.actorGO.transform.position = new Vector3(actorPosition.actorGO.transform.position.x + actorPosition.offset.x * 2, actorPosition.actorGO.transform.position.y, actorPosition.actorGO.transform.position.z);
                }
                actorPosition.actorGO.transform.localScale = new Vector3(Mathf.Abs(actorPosition.actorGO.transform.localScale.x), actorPosition.actorGO.transform.localScale.y, actorPosition.actorGO.transform.localScale.z);
                actorPosition.isTurningBack = false;
            }
            if (!actorPosition.isTurningBack)
            {
                tmpDestination = new Vector2(tmpDestination.x + actorPosition.offset.x, tmpDestination.y + actorPosition.offset.y);
            }
            else
            {
                tmpDestination = new Vector2(tmpDestination.x - actorPosition.offset.x, tmpDestination.y + actorPosition.offset.y);
            }
            Vector2 tmpOrigin = actorPosition.actorGO.transform.position;
            yield return MoveFromTo(actorPosition.actorGO.transform, tmpOrigin, tmpDestination, actorPosition.speed);
            SetGameObjectSortingOrder(actorPosition.actorGO, path[i].destPath.y);
        }
        actorPosition.position = dest;
        if (actorPosition.actorGO.name == playerGO.name)
        {
            BattleState.instance.SetCurrentStateTo("PlayerMovementHighlightCells");
        }
        anim.SetTrigger("Moving");
        yield return null;
    }

    /// <summary>
    /// This function moves the actor in parameter toward the direction of another actor using a limited number of move
    /// </summary>
    /// <param name="actorPosition">The actor to move</param>
    /// <param name="actorToMoveToward">The targeted actor to move toward</param>
    /// <param name="nbOfMove">The max number of move</param>
    public IEnumerator MoveCharacterTowards(ActorPosition actorPosition, ActorPosition actorToMoveToward, int nbOfMove)
    {
        List<Vector2Int> emptyHexInRadius = GetEmptyHexInRadius(actorPosition.position, nbOfMove);
        Vector2Int dest = new Vector2Int(-1, -1);
        for (int i = 1; i < map.sizeX; i++)
        {
            List<Vector2Int> emptyHexInActorToMoveTowardRadius = GetEmptyHexInRadius(actorToMoveToward.position, i);
            for (int j = 0; j < emptyHexInRadius.Count; j++)
            {
                for (int k = 0; k < emptyHexInActorToMoveTowardRadius.Count; k++)
                {
                    if (emptyHexInRadius[j].x == emptyHexInActorToMoveTowardRadius[k].x && emptyHexInRadius[j].y == emptyHexInActorToMoveTowardRadius[k].y)
                    {
                        dest = new Vector2Int(emptyHexInRadius[j].x, emptyHexInRadius[j].y);
                        break;
                    }
                }
                if (dest.x != -1)
                {
                    break;
                }
            }
            if (dest.x != -1)
            {
                break;
            }
        }
        if (dest.x != -1)
        {
            yield return MoveCharacterTo(actorPosition, dest);
        }
        else
        {
            yield return null;
        }
    }

    /// <summary>
    /// This function takes care of moving a game object using world coordinates
    /// </summary>
    /// <param name="objectToMove">The transform of the game object to move</param>
    /// <param name="a">The world coordinate we want to move the object from</param>
    /// <param name="b">The world coordinate we want to move the object to</param>
    /// <param name="speed">How fast the object is moving from a to b</param>
    public IEnumerator MoveFromTo(Transform objectToMove, Vector2 a, Vector2 b, float speed)
    {
        float initialZ = objectToMove.position.z;
        float step = (speed / (a - b).magnitude) * Time.fixedDeltaTime;
        float t = 0;
        while (t <= 1.0f)
        {
            t += step; // Goes from 0 to 1, incrementing by step each time
            objectToMove.position = Vector2.Lerp(a, b, t); // Move objectToMove closer to b
            yield return new WaitForFixedUpdate(); // Leave the routine and return here in the next frame
        }
        objectToMove.position = new Vector3(b.x, b.y, initialZ);
    }

    /// <summary>
    /// (Prototyping logic)
    /// When moving a card, a line is drawn between the card and the finger.
    /// This function detects which hexagon is under the finger while drawing this line
    /// </summary>
    private GameObject GetHexUnderDrawingLine(Vector2 fingerPosition)
    {
        foreach (GameObject hexagonGO in hexagonsObjectList)
        {
            PolygonCollider2D collider = hexagonGO.transform.GetChild(0).gameObject.GetComponent<PolygonCollider2D>();
            if (collider.OverlapPoint(fingerPosition))
            {
                return hexagonGO;
            }
        }
        return null;
    }
}

public class ObstacleListItem
{
    public ObstacleListItem(Vector2Int obsPosition, int obsId)
    {
        obstaclePosition = obsPosition;
        obstacleId = obsId;
    }
    public Vector2Int obstaclePosition;
    public int obstacleId;
}

public class HexPath
{
    public HexPath(Vector2Int origin, Vector2Int dest)
    {
        originPath = origin;
        destPath = dest;
    }
    public Vector2Int originPath;
    public Vector2Int destPath;
    public bool IsDestinationPathEqualTo(Vector2Int destToCheck)
    {
        if (destToCheck.x == destPath.x && destToCheck.y == destPath.y)
        {
            return true;
        }
        return false;
    }

    public bool IsPathEqualTo(Vector2Int originToCheck, Vector2Int destToCheck)
    {
        if (originToCheck.x == originPath.x && originToCheck.y == originPath.y && destToCheck.x == destPath.x && destToCheck.y == destPath.y)
        {
            return true;
        }
        return false;
    }

}

public class ActorPosition
{
    public GameObject actorGO;
    public Vector2Int position;
    public bool isTurningBack;
    public Vector2 offset;
    public float speed;
    public ActorPosition(GameObject go, Vector2Int pos, bool turnBack, Vector2 os, float speedActor)
    {
        actorGO = go;
        position = pos;
        isTurningBack = turnBack;
        offset = os;
        speed = speedActor;
    }
}