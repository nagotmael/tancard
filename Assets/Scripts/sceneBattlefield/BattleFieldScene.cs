﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BattleFieldScene : MonoBehaviour
{
    public ListOfMaps listOfMaps;
    [SerializeField]
    private GameObject rootObject;
    private Map map;
    private string state;
    private Encounter encounter;
    [SerializeField]
    private ListOfAllies listOfAllies;
    private List<Ally> allies;
    private Actor currentActor;

    /// <summary>
    /// Initialize the battlefield when the scene is loaded
    /// </summary>
    void Start()
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Battlefield"));
        encounter = DataController.instance.currentEncounter;
        string mapName = DataController.instance.GetCurrentMap();
        map = listOfMaps.GetMap(mapName);
        allies = listOfAllies.GetAllAlliesFromInt(DataController.instance.gameData.allies);
        LoadBattleSound(map);
        LoadBattleBackground();
        HeroEquipment.instance.EquipHero();
        InitializeGridController();
        InitializeActor("hero", 0);
        GridController.instance.CreateGrid();
        GridController.instance.GenerateObstacles();
        GridController.instance.DisplayObstacles();
        GridController.instance.InitializeActorsPosition();
        BattleState.instance.SetCurrentStateTo("PlayerMovementHighlightCells");
        StartCoroutine(BattleControlLoop());
    }

    private void InitializeGridController()
    {
        GridController.instance.rootObject = rootObject;
        GridController.instance.listOfMaps = listOfMaps;
        GridController.instance.map = map;
        GridController.instance.encounter = encounter;
        GridController.instance.allies = allies;
    }

    private void InitializeActor(string type, int id)
    {
        currentActor = new Actor(type, id);
    }

    /// <summary>
    /// This function loads the background music and the background ambiance of the battlefield
    /// </summary>
    private void LoadBattleSound(Map map)
    {
        StartCoroutine(SoundController.instance.PlayBackgroundMusic(map.bgmFileBattle, 1, 1));
        StartCoroutine(SoundController.instance.PlayBackgroundSound(map.bgsFileBattle, 1, 1));
    }

    public void LoadBattleBackground()
    {
        GameObject backgroundObject = GameObject.Instantiate(map.battleBackground);
        backgroundObject.transform.SetParent(rootObject.transform);
    }

    /// <summary>
    /// This function controls the battle logic. When actor has finished its turn, moves to next actor until the battle is finished
    /// </summary>
    private IEnumerator BattleControlLoop()
    {
        while (true)
        {
            if (currentActor.type == "hero")
            {
                yield return StartCoroutine(PlayerTurn());
            }

            if (currentActor.type == "ally")
            {
                yield return StartCoroutine(PlayerTurn());
            }

            if (currentActor.type == "enemy")
            {
                yield return StartCoroutine(EnemyTurn());
            }
            MoveToNextActor();
            yield return null;
        }
    }

    /// <summary>
    /// This function decided which actor has its turn next
    /// </summary>
    private void MoveToNextActor()
    {
        if (currentActor.type == "hero")
        {
            if (GridController.instance.alliesPosition != null)
            {
                currentActor.type = "ally";
                currentActor.id = 0;
            }
            else
            {
                currentActor.type = "enemy";
                currentActor.id = 0;
            }
        }
        else if (currentActor.type == "ally")
        {
            if (currentActor.id == GridController.instance.alliesPosition.Count - 1)
            {
                currentActor.type = "enemy";
                currentActor.id = 0;
            }
            else
            {
                currentActor.id++;
            }
        }
        else if (currentActor.type == "enemy")
        {
            if (currentActor.id == GridController.instance.enemiesPosition.Count - 1)
            {
                currentActor.type = "hero";
                currentActor.id = 0;
            }
            else
            {
                currentActor.id++;
            }
        }
    }

    /// <summary>
    /// This function controls the logic of the turn of the hero or an ally
    /// </summary>
    private IEnumerator PlayerTurn()
    {
        SelectActorHex();
        HighlightHexAroundActor();
        yield return StartCoroutine(WaitForState("HexSelected"));
        GridController.instance.RemoveHightlightAllTiles();
        GridController.instance.RemoveSelectAllTiles();
        if (currentActor.type == "hero")
        {
            yield return StartCoroutine(GridController.instance.MoveCharacterTo(GridController.instance.playerPosition, BattleState.instance.hex));
        }
        else
        {
            yield return StartCoroutine(GridController.instance.MoveCharacterTo(GridController.instance.alliesPosition[currentActor.id], BattleState.instance.hex));
        }
        BattleState.instance.SetCurrentStateTo("Idle");
    }

    /// <summary>
    /// This function controls the logic of the enemies
    /// </summary>
    private IEnumerator EnemyTurn()
    {
        yield return StartCoroutine(GridController.instance.MoveCharacterTowards(GridController.instance.enemiesPosition[currentActor.id], GridController.instance.playerPosition, 3));
    }

    /// <summary>
    /// This function highlights the hexagons around an actor
    /// It is now used for prototyping only and shows where the actor can move to.
    /// </summary>
    private void HighlightHexAroundActor()
    {
        if (currentActor.type == "hero")
        {
            GridController.instance.HighlightEmptyHexInRadius(GridController.instance.playerPosition.position, 5);
        }
        else
        {
            GridController.instance.HighlightEmptyHexInRadius(GridController.instance.alliesPosition[currentActor.id].position, 5);
        }
    }

    /// <summary>
    /// This function is used to select the hexagon of the current actor so the player understands it is its turn.
    /// </summary>
    private void SelectActorHex()
    {
        if (currentActor.type == "hero")
        {
            GridController.instance.SelectTile(GridController.instance.playerPosition.position);
        }
        else
        {
            GridController.instance.SelectTile(GridController.instance.alliesPosition[currentActor.id].position);
        }
    }


    /// <summary>
    /// This function is used to wait for a state to be triggered
    /// For example, when an actor is moving, when the move is completed, it triggers a specific state.
    /// This function would prevent the player to do any input until the move is completed and hence the state is triggered
    /// </summary>
    /// <param name="state">The state to wait for</param>
    private IEnumerator WaitForState(string state)
    {
        while (true)
        {
            if (BattleState.instance.GetCurrentState() == state)
            {
                break;
            }
            yield return null;
        }
    }
}

public class Actor
{
    public string type;
    public int id;
    public Actor(string typeActor, int idActor)
    {
        type = typeActor;
        id = idActor;
    }
}