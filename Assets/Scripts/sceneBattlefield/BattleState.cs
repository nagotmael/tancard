﻿using UnityEngine;

public class BattleState : MonoBehaviour
{
    public static BattleState instance;
    private string currentState;
    public Vector2Int hex;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public string GetCurrentState()
    {
        return currentState;
    }

    public void SetCurrentStateTo(string state)
    {
        currentState = state;
    }
}
