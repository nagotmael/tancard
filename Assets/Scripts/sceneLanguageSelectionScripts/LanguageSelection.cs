﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LanguageSelection : MonoBehaviour
{
    public Button buttonFrench;
    public Button buttonEnglish;
    [SerializeField]
    private GameObject textLanguage;

    void Awake()
    {
        // Make the flag buttons loading the localization when clicking on them
        buttonFrench.onClick.AddListener(() => LoadLocalization("fr"));
        buttonEnglish.onClick.AddListener(() => LoadLocalization("en"));
    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.2f);

        // Tween to make the flags and the text fade in when the scene is loaded
        Sequence flagFadeIn = DOTween.Sequence();
        flagFadeIn
            .Append(textLanguage.GetComponent<Text>().DOFade(1, 1))
            .Join(buttonFrench.gameObject.GetComponent<Image>().DOFade(1, 1))
            .Join(buttonEnglish.gameObject.GetComponent<Image>().DOFade(1, 1));
        // Tween for looping the flag animation
        Sequence flagAnimation = DOTween.Sequence();
        flagAnimation
            .Append(buttonFrench.gameObject.GetComponent<Image>().transform.DOScale(new Vector3(0.8f, 0.8f, 1), 1))
            .Join(buttonEnglish.gameObject.GetComponent<Image>().transform.DOScale(new Vector3(0.8f, 0.8f, 1), 1))
            .SetLoops(-1, LoopType.Yoyo);
        StartCoroutine(FadeOutFlagsAndLoadScene());
    }

    /// <summary>
    /// This method make the flag fade out before loading the menu.
    /// </summary>  
    IEnumerator FadeOutFlagsAndLoadScene()
    {
        while (!LocalizationController.instance.GetIsReady())
        {
            yield return null;
        }
        // Tween to fade out the flags and the text once the localization is fully loaded
        Sequence flagFadeOut = DOTween.Sequence();
        flagFadeOut
            .Append(textLanguage.GetComponent<Text>().DOFade(0, 1))
            .Join(buttonFrench.gameObject.GetComponent<Image>().DOFade(0, 1))
            .Join(buttonEnglish.gameObject.GetComponent<Image>().DOFade(0, 1));
        yield return new WaitForSeconds(1);
        yield return StartCoroutine(LoadingScreenController.instance.LoadScene("Menu"));
    }

    /// <summary>
    /// Loading Localization and saving the language choice in the game data json file
    /// </summary>  
    private void LoadLocalization(string language)
    {
        LocalizationController.instance.LoadLocalizedText("localizedText_" + language + ".json");
        DataController.instance.gameData.localizationLanguage = language;
        DataController.instance.SaveGameData();
    }

}