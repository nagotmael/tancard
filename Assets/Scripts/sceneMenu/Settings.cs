﻿using UnityEngine;

public class Settings : MonoBehaviour
{
    /// <summary>
    /// Called when the settings button is tapped from the menu
    /// </summary>
    public void OnSettingsTap()
    {
        Debug.Log("Tap on Settings detected");
    }

    /// <summary>
    /// Called when long tapping the settings button from the menu.
    /// This function is used to display the tooltip on the settings button.
    /// </summary>
    public void OnSettingsLongTapDown()
    {
        UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("settings_game_menu"));
    }

    /// <summary>
    /// Called after stopping long tapping the settings button from the menu.
    /// It is used to hide the tooltip when stopping to long tap on the button.
    /// </summary>
    public void OnSettingsLongTapUp()
    {
        UIController.instance.HideToolTip();
    }
}
