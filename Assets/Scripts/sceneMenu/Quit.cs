﻿using System.Collections;
using UnityEngine;

public class Quit : MonoBehaviour
{

    /// <summary>
    /// This method is called when clicking the quit button
    /// </summary>
    public void OnQuitGameTap()
    {
        StartCoroutine("OnQuitGameTapCoroutine");
    }

    /// <summary>
    /// This method open a modal panel for the user to confirm if he really wants to quit the game
    /// </summary>
    private IEnumerator OnQuitGameTapCoroutine()
    {
        PlayClickSound();
        StartCoroutine(UIController.instance.ModalPanelChoice("quit_confirm", new string[] { "yes", "no" }));
        yield return new WaitForSeconds(0.1f);
        while (UIController.instance.modalPanelAnswer == null)
        {
            yield return null;
        }
        PlayClickSound();
        string answer = UIController.instance.modalPanelAnswer;
        if (answer == "yes")
        {
            Application.Quit();
        }
    }

    /// <summary>
    /// Called when long tapping the quit button from the menu.
    /// This function is used to display the tooltip on the quit button.
    /// </summary>
    public void OnQuitGameLongTapDown()
    {
        UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("quit_game_menu"));
    }


    /// <summary>
    /// Called after stopping long tapping the quit button from the menu.
    /// It is used to hide the tooltip when stopping to long tap on the button.
    /// </summary>
    public void OnQuitGameLongTapUp()
    {
        UIController.instance.HideToolTip();
    }
    private void PlayClickSound()
    {
        StartCoroutine(SoundController.instance.PlaySE(SoundContainerMenu.instance.clickSound, 1));
    }

}
