﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{

    public Button buttonQuit;
    public Button buttonNewGame;
    public Button buttonContinueGame;
    public Button buttonHelp;
    public Button buttonSettings;
    [SerializeField]
    private ParticleSystem particleSys;

    void Awake()
    {
        buttonNewGame.onClick.AddListener(() => StartCoroutine(OnStartNewGameTap()));
        buttonContinueGame.onClick.AddListener(() => StartCoroutine(OnContinueNewGameTap()));
    }
    IEnumerator Start()
    {
        // Tween to make the menu items slightly changing color over time
        Sequence menuItemAnimation = DOTween.Sequence();
        Color menuItemTargetColor = new Color32(79, 42, 8, 220);
        if (!DataController.instance.gameData.isGameSessionStarted)
        {
            // If no game ongoing, deactivate continue button
            buttonContinueGame.interactable = false;
            Color buttonTextColor = buttonContinueGame.GetComponentInChildren<Text>().color;
            buttonTextColor.a = 0.5f;
            buttonContinueGame.GetComponentInChildren<Text>().color = buttonTextColor;
            menuItemAnimation
                    .Append(buttonNewGame.gameObject.GetComponentInChildren<Text>().DOColor(menuItemTargetColor, 2))
                    .SetLoops(-1, LoopType.Yoyo);
        }
        else
        {
            menuItemAnimation
                    .Append(buttonNewGame.gameObject.GetComponentInChildren<Text>().DOColor(menuItemTargetColor, 2))
                    .Join(buttonContinueGame.gameObject.GetComponentInChildren<Text>().DOColor(menuItemTargetColor, 2))
                    .SetLoops(-1, LoopType.Yoyo);
        }

        // Tween to make the Particles system displaying the leaves moving right and left so the leaves spread everywhere
        Sequence leaveParticlesSystemAnimation = DOTween.Sequence();
        float xParticleSytem = particleSys.transform.position.x;
        leaveParticlesSystemAnimation
                .Append(particleSys.transform.DOMoveX(xParticleSytem - 19, 1))
                .Append(particleSys.transform.DOMoveX(xParticleSytem, 1))
                .SetLoops(-1);
        yield return null;
    }

    void Update()
    {

    }

    private void PlayClickSound()
    {
        StartCoroutine(SoundController.instance.PlaySE(SoundContainerMenu.instance.clickSound, 1));
    }

    /// <summary>
    /// Called after clicking on the "New Game" button.
    /// It loads the new game data and load the map scene.
    /// If a game was ongoing it also opens a modal panel to confirm previous game data deletion.
    /// </summary>
    public IEnumerator OnStartNewGameTap()
    {
        PlayClickSound();
        if (DataController.instance.gameData.isGameSessionStarted)
        {
            StartCoroutine(UIController.instance.ModalPanelChoice("new_game_confirm", new string[] { "yes", "no" }));
            yield return new WaitForSeconds(0.1f);
            while (UIController.instance.modalPanelAnswer == null)
            {
                yield return null;
            }
            PlayClickSound();
            string answer = UIController.instance.modalPanelAnswer;
            if (answer == "no")
            {
                yield break;
            }
        }

        InitializeGameData();
        yield return StartCoroutine(LoadingScreenController.instance.LoadScene("Map"));
    }

    /// <summary>
    /// Called after clicking on the "Continue" button.
    /// It calls the map scene.
    /// </summary>
    public IEnumerator OnContinueNewGameTap()
    {
        yield return StartCoroutine(LoadingScreenController.instance.LoadScene("Map"));
    }

    /// <summary>
    /// This method initializes the game data
    /// TBD Create a scriptable object to make the initial values configurable from the editor instead of from the code
    /// </summary>
    private void InitializeGameData()
    {
        DataController.instance.gameData.isGameSessionStarted = true;
        DataController.instance.gameData.level = 1;
        DataController.instance.gameData.currentMap = "Forest";
        DataController.instance.gameData.mapItemLinks = null;
        DataController.instance.gameData.mapTiles = null;
        DataController.instance.gameData.allies = new List<int>();
        DataController.instance.gameData.allies.Add(1);
        InitializeStartingEquipment();
        InitializeAttributes();
    }

    private void InitializeStartingEquipment()
    {
        DataController.instance.gameData.gold = 800;
        DataController.instance.gameData.equipment.inventoryItems[0] = 2;
        DataController.instance.gameData.equipment.inventoryItems[1] = 3;
        DataController.instance.gameData.equipment.inventoryItems[2] = 4;
        DataController.instance.gameData.equipment.inventoryItems[3] = 6;
        DataController.instance.gameData.equipment.inventoryItems[4] = 7;
        DataController.instance.gameData.equipment.inventoryItems[5] = 0;
        DataController.instance.gameData.equipment.inventoryItems[6] = 0;
        DataController.instance.gameData.equipment.inventoryItems[7] = 0;
        DataController.instance.gameData.equipment.inventoryItems[8] = 0;
        DataController.instance.gameData.equipment.frontHandItem = 6;
        DataController.instance.gameData.equipment.shoesItem = 5;
        DataController.instance.gameData.equipment.armorItem = 1;
        DataController.instance.gameData.equipment.helmetItem = 0;
        DataController.instance.gameData.equipment.backHandItem = 0;
        DataController.instance.gameData.equipment.leftRing = 0;
        DataController.instance.gameData.equipment.rightRing = 0;
    }

    private void InitializeAttributes()
    {
        DataController.instance.gameData.attributes.strength = 6;
        DataController.instance.gameData.attributes.dexterity = 6;
        DataController.instance.gameData.attributes.intelligence = 6;
        DataController.instance.gameData.attributes.maxHp = 50;
        DataController.instance.gameData.attributes.maxMp = 40;
        DataController.instance.gameData.attributes.hp = 35;
        DataController.instance.gameData.attributes.mp = 23;
    }
}
