﻿using UnityEngine;

public class InventoryUse : MonoBehaviour
{
    /// <summary>
    /// This method is called when clicking the use button
    /// </summary>
    public void OnUseTap()
    {
        InventoryController.instance.UseSelectedObject();
        StartCoroutine("PlayClickSound");
    }

    /// <summary>
    /// Called when long tapping the use button
    /// </summary>
    public void OnUseLongTapDown()
    {
        UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("use_button"));
    }


    /// <summary>
    /// Called after stopping long tapping the use button.
    /// </summary>
    public void OnUseLongTapUp()
    {
        UIController.instance.HideToolTip();
    }

    private void PlayClickSound()
    {
        StartCoroutine(SoundController.instance.PlaySE(SoundContainerMap.instance.clickSound, 1));
    }
}
