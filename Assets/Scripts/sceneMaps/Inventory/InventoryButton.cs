﻿using UnityEngine;

public class InventoryButton : MonoBehaviour
{
    public GameObject inventory;

    /// <summary>
    /// This method is called when clicking the inventory button
    /// </summary>
    public void OnInventoryTap()
    {
        StartCoroutine("PlayClickSound");
        inventory.SetActive(true);
    }

    /// <summary>
    /// Called when long tapping the inventory button
    /// This function is used to display the tooltip on the inventory button.
    /// </summary>
    public void OnInventoryLongTapDown()
    {
        Debug.Log("Inventory Long Tap Down");
        UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("inventory"));
    }

    /// <summary>
    /// Called after stopping long tapping the inventory button.
    /// It is used to hide the tooltip when stopping to long tap on the button.
    /// </summary>
    public void OnInventoryLongTapUp()
    {
        UIController.instance.HideToolTip();
    }

    private void PlayClickSound()
    {
        StartCoroutine(SoundController.instance.PlaySE(SoundContainerMap.instance.clickSound, 1));
    }
}