﻿using UnityEngine;

public class InventoryThrowAway : MonoBehaviour
{
    /// <summary>
    /// This method is called when clicking the throw away button
    /// </summary>
    public void OnThrowAwayTap()
    {
        StartCoroutine(InventoryController.instance.ThrowAwaySelectedObject());
        StartCoroutine("PlayClickSound");
    }

    /// <summary>
    /// Called when long tapping the throw away button
    /// </summary>
    public void OnThrowAwayLongTapDown()
    {
        UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("throw_away_button"));
    }


    /// <summary>
    /// Called after stopping long tapping the throw away button.
    /// </summary>
    public void OnThrowAwayLongTapUp()
    {
        UIController.instance.HideToolTip();
    }

    private void PlayClickSound()
    {
        StartCoroutine(SoundController.instance.PlaySE(SoundContainerMap.instance.clickSound, 1));
    }
}
