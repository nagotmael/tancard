﻿using UnityEngine;

public class InventoryButtonClose : MonoBehaviour
{
    public GameObject inventory;

    /// <summary>
    /// This method is called when clicking the close inventory button
    /// </summary>
    public void OnInventoryCloseTap()
    {
        StartCoroutine("PlayClickSound");
        inventory.SetActive(false);
    }

    /// <summary>
    /// Called when long tapping the close inventory button
    /// This function is used to display the tooltip on the inventory button.
    /// </summary>
    public void OnInventoryCloseLongTapDown()
    {
        UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("close_inventory"));
    }


    /// <summary>
    /// Called after stopping long tapping the close inventory button.
    /// It is used to hide the tooltip when stopping to long tap on the button.
    /// </summary>
    public void OnInventoryCloseLongTapUp()
    {
        UIController.instance.HideToolTip();
    }

    private void PlayClickSound()
    {
        StartCoroutine(SoundController.instance.PlaySE(SoundContainerMap.instance.clickSound, 1));
    }
}