﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Lean.Touch;

// The solution taken for the inventory is not good enough
// The code has to be rewritten from scratch
// It should be written in a way that it is agnostic to the number of slots available in the inventory
// This way, it will allow the gameplay to be much more flexible
// Also it will avoid this long list of "if" statements testing the value of hardcoded field
public class InventoryController : MonoBehaviour
{
    public static InventoryController instance;
    [SerializeField]
    private GameObject goldText;
    public ListOfItems listOfItems;
    [SerializeField]
    private GameObject throwAwayButton;
    [SerializeField]
    private GameObject useButton;
    [SerializeField]
    private GameObject[] slots = new GameObject[16];
    [SerializeField]
    private Text strengthValueText;
    [SerializeField]
    private Text dexterityValueText;
    [SerializeField]
    private Text intelligenceValueText;
    [SerializeField]
    private Text hpValueText;
    [SerializeField]
    private Text mpValueText;
    public int selectedSlot = -1;
    [SerializeField]
    private Slider hpSlider;
    [SerializeField]
    private Slider mpSlider;

    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        ReloadInventory();
    }

    /// <summary>
    /// Reload all the values and slots in the inventory.
    /// Is called when the inventory is opened and whenever the player do an action in the inventory
    /// </summary>
    private void ReloadInventory()
    {
        DeselectAllSlots();
        UnHighlightAllSlots();
        goldText.GetComponent<Text>().text = DataController.instance.gameData.gold.ToString();
        LoadUnequippedSlots();
        LoadEquippedSlots();
        ReloadStrengthValueText();
        ReloadDexterityValueText();
        ReloadIntelligenceValueText();
        ReloadHpValue();
        ReloadMpValue();
    }

    /// <summary>
    /// Whenever the inventory is reloaded, this function update the strength text as well as the strength bonus/malus if any
    /// </summary>
    private void ReloadStrengthValueText()
    {
        int strengthBonus = DataController.instance.GetStrengthBonus();

        string strengthTextToDisplay = "";
        if (strengthBonus > 0) { strengthTextToDisplay += "<color=green>"; }
        if (strengthBonus == 0) { strengthTextToDisplay += "<color=white>"; }
        if (strengthBonus < 0) { strengthTextToDisplay += "<color=red>"; }
        strengthTextToDisplay += (DataController.instance.gameData.attributes.strength + strengthBonus).ToString();
        if (strengthBonus != 0) { strengthTextToDisplay += "</color><color=white> (" + DataController.instance.gameData.attributes.strength + ")"; }
        strengthTextToDisplay += "</color>";
        strengthValueText.text = strengthTextToDisplay;
    }

    /// <summary>
    /// Whenever the inventory is reloaded, this function update the dexterity text as well as the dexterity bonus/malus if any
    /// </summary>
    private void ReloadDexterityValueText()
    {
        int dexterityBonus = DataController.instance.GetDexterityBonus(); ;
        string dexterityTextToDisplay = "";
        if (dexterityBonus > 0) { dexterityTextToDisplay += "<color=green>"; }
        if (dexterityBonus == 0) { dexterityTextToDisplay += "<color=white>"; }
        if (dexterityBonus < 0) { dexterityTextToDisplay += "<color=red>"; }
        dexterityTextToDisplay += (DataController.instance.gameData.attributes.dexterity + dexterityBonus).ToString();
        if (dexterityBonus != 0) { dexterityTextToDisplay += "</color><color=white> (" + DataController.instance.gameData.attributes.dexterity + ")"; }
        dexterityTextToDisplay += "</color>";
        dexterityValueText.text = dexterityTextToDisplay;
    }

    /// <summary>
    /// Whenever the inventory is reloaded, this function update the intelligence text as well as the intelligence bonus/malus if any
    /// </summary>
    private void ReloadIntelligenceValueText()
    {
        int intelligenceBonus = DataController.instance.GetIntelligenceBonus();
        string intelligenceTextToDisplay = "";
        if (intelligenceBonus > 0) { intelligenceTextToDisplay += "<color=green>"; }
        if (intelligenceBonus == 0) { intelligenceTextToDisplay += "<color=white>"; }
        if (intelligenceBonus < 0) { intelligenceTextToDisplay += "<color=red>"; }
        intelligenceTextToDisplay += (DataController.instance.gameData.attributes.intelligence + intelligenceBonus).ToString();
        if (intelligenceBonus != 0) { intelligenceTextToDisplay += "</color><color=white> (" + DataController.instance.gameData.attributes.intelligence + ")"; }
        intelligenceTextToDisplay += "</color>";
        intelligenceValueText.text = intelligenceTextToDisplay;
    }

    /// <summary>
    /// Whenever the inventory is reloaded, this function update the health point text as well as the health point bonus/malus if any
    /// </summary>
    private void ReloadHpValue()
    {
        int hpMaxBonus = DataController.instance.GetMaxHpBonus();
        int hpToDisplay = DataController.instance.gameData.attributes.hp;
        if (hpToDisplay > DataController.instance.gameData.attributes.maxHp + hpMaxBonus) { hpToDisplay = DataController.instance.gameData.attributes.maxHp + hpMaxBonus; }
        string hpMaxBonusTextToDisplay = "";
        hpMaxBonusTextToDisplay += hpToDisplay + "/";
        if (hpMaxBonus > 0) { hpMaxBonusTextToDisplay += "<color=green>"; }
        if (hpMaxBonus == 0) { hpMaxBonusTextToDisplay += "<color=white>"; }
        if (hpMaxBonus < 0) { hpMaxBonusTextToDisplay += "<color=red>"; }
        hpMaxBonusTextToDisplay += (DataController.instance.gameData.attributes.maxHp + hpMaxBonus).ToString();
        if (hpMaxBonus != 0) { hpMaxBonusTextToDisplay += "</color><color=white> (" + DataController.instance.gameData.attributes.maxHp + ")"; }
        hpMaxBonusTextToDisplay += "</color>";
        hpValueText.text = hpMaxBonusTextToDisplay;
        hpSlider.value = (float)DataController.instance.gameData.attributes.hp / (float)(DataController.instance.gameData.attributes.maxHp + hpMaxBonus);
    }

    /// <summary>
    /// Whenever the inventory is reloaded, this function update the magic points text as well as the magic points bonus/malus if any
    /// </summary>
    private void ReloadMpValue()
    {
        int mpMaxBonus = DataController.instance.GetMaxMpBonus();
        int mpToDisplay = DataController.instance.gameData.attributes.mp;
        if (mpToDisplay > DataController.instance.gameData.attributes.maxMp + mpMaxBonus) { mpToDisplay = DataController.instance.gameData.attributes.maxMp + mpMaxBonus; }
        string mpMaxBonusTextToDisplay = "";
        mpMaxBonusTextToDisplay += mpToDisplay + "/";
        if (mpMaxBonus > 0) { mpMaxBonusTextToDisplay += "<color=green>"; }
        if (mpMaxBonus == 0) { mpMaxBonusTextToDisplay += "<color=white>"; }
        if (mpMaxBonus < 0) { mpMaxBonusTextToDisplay += "<color=red>"; }
        mpMaxBonusTextToDisplay += (DataController.instance.gameData.attributes.maxMp + mpMaxBonus).ToString();
        if (mpMaxBonus != 0) { mpMaxBonusTextToDisplay += "</color><color=white> (" + DataController.instance.gameData.attributes.maxMp + ")"; }
        mpMaxBonusTextToDisplay += "</color>";
        mpValueText.text = mpMaxBonusTextToDisplay;
        mpSlider.value = (float)DataController.instance.gameData.attributes.mp / (float)(DataController.instance.gameData.attributes.maxMp + mpMaxBonus);
    }

    /// <summary>
    /// This functions shows the items that are in the inventory but that are not equipped
    /// </summary>
    private void LoadUnequippedSlots()
    {
        int[] inventoryItems = DataController.instance.gameData.equipment.inventoryItems;
        for (int i = 0; i < inventoryItems.Length; i++)
        {
            Color color = Color.white;
            color.a = 0;
            slots[i].transform.GetChild(0).gameObject.GetComponent<Image>().color = color;
            if (slots[i].transform.childCount > 1)
            {
                Destroy(slots[i].transform.GetChild(1).gameObject);
            }
            if (inventoryItems[i] != 0)
            {
                ShowRarity(slots[i].transform.GetChild(0).gameObject.GetComponent<Image>(), listOfItems.GetItem(inventoryItems[i]));
                GameObject.Instantiate(listOfItems.GetItem(inventoryItems[i]).inventorySprite, slots[i].transform);
            }
        }
    }

    /// <summary>
    /// This function takes care of colorizing the inventory slot in another color for rare items
    /// </summary>
    /// <param name="image">The background image of the inventory slot to be colorized</param>
    /// <param name="item">The item we want to show rarity about</param>
    private void ShowRarity(Image image, Item item)
    {
        Color32 color = new Color();
        if ((int)item.rarity == 0)
        {
            color = Color.white;
            color.a = 0;
        }
        else if ((int)item.rarity == 1)
        {
            color.r = 25;
            color.g = 58;
            color.b = 126;
            color.a = 91;
        }
        else if ((int)item.rarity == 2)
        {
            color.r = 121;
            color.g = 121;
            color.b = 20;
            color.a = 91;
        }
        image.color = color;
    }

    /// <summary>
    /// This functions shows the items that are equipped by the hero in the inventory
    /// </summary>
    private void LoadEquippedSlots()
    {
        Equipment equipment = DataController.instance.gameData.equipment;
        // Load Slot 9 Left Hand
        LoadEquippedItemOnSlot(9, equipment.frontHandItem);

        // Load Slot 10 Right Hand
        LoadEquippedItemOnSlot(10, equipment.backHandItem);

        // Load Slot 11 Armor
        LoadEquippedItemOnSlot(11, equipment.armorItem);

        // Load Slot 12 Shoes
        LoadEquippedItemOnSlot(12, equipment.shoesItem);

        // Load Slot 13 Left Ring
        LoadEquippedItemOnSlot(13, equipment.leftRing);

        // Load Slot 14 Right Ring
        LoadEquippedItemOnSlot(14, equipment.rightRing);

        // Load Slot 15 Helmet
        LoadEquippedItemOnSlot(15, equipment.helmetItem);
    }

    /// <summary>
    /// This functions shows the items that are equipped by the hero on a particular slot   
    /// </summary>
    /// <param name="slotId">9 = Left hand, 10 = Right hand, 11 = Armor, 12 = Shoes, 13 = Left ring, 14 = Right ring, 15 = Helmet</param>
    /// <param name="item">The id of the item to equip on the slot</param>
    private void LoadEquippedItemOnSlot(int slotId, int item)
    {
        Color color = Color.white;
        color.a = 0;
        slots[slotId].transform.GetChild(1).gameObject.GetComponent<Image>().color = color;
        if (slots[slotId].transform.childCount > 2)
        {
            Destroy(slots[slotId].transform.GetChild(2).gameObject);
        }
        if (item != 0)
        {
            ShowRarity(slots[slotId].transform.GetChild(1).gameObject.GetComponent<Image>(), listOfItems.GetItem(item));
            slots[slotId].transform.GetChild(0).gameObject.SetActive(false);
            GameObject.Instantiate(listOfItems.GetItem(item).inventorySprite, slots[slotId].transform);
        }
        else
        {
            slots[slotId].transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// This functions deselect a slot and remove the green border around the slot 
    /// </summary>
    /// <param name="slotId">1-8 = unequipped slot, 9 = Left hand, 10 = Right hand, 11 = Armor, 12 = Shoes, 13 = Left ring, 14 = Right ring, 15 = Helmet</param>
    public void DeselectSlot(int slotId)
    {
        slots[slotId].GetComponent<InventorySlot>().DeselectSlot();
    }

    /// <summary>
    /// Whenever an unequipped slot of the inventory is selected, this function highlight the correspoding equipped slot by putting an orange border
    /// For example, if the player select an unequipped sword, this function will highlight the left hand and right hand slots
    /// </summary>
    public void HighlightCorrespondingSlots()
    {
        if (selectedSlot >= 0 && selectedSlot <= 8)
        {
            if (DataController.instance.gameData.equipment.inventoryItems[selectedSlot] != 0)
            {
                Item itemSelected = listOfItems.GetItem(DataController.instance.gameData.equipment.inventoryItems[selectedSlot]);
                if ((int)itemSelected.itemType == 0)
                {
                    slots[9].GetComponent<InventorySlot>().HighlightSlot();
                    slots[10].GetComponent<InventorySlot>().HighlightSlot();
                }
                if ((int)itemSelected.itemType == 1)
                {
                    slots[9].GetComponent<InventorySlot>().HighlightSlot();
                }
                if ((int)itemSelected.itemType == 2)
                {
                    slots[11].GetComponent<InventorySlot>().HighlightSlot();
                }
                if ((int)itemSelected.itemType == 3)
                {
                    slots[10].GetComponent<InventorySlot>().HighlightSlot();
                }
                if ((int)itemSelected.itemType == 4)
                {
                    slots[15].GetComponent<InventorySlot>().HighlightSlot();
                }
                if ((int)itemSelected.itemType == 5)
                {
                    slots[12].GetComponent<InventorySlot>().HighlightSlot();
                }
                if ((int)itemSelected.itemType == 6)
                {
                    slots[13].GetComponent<InventorySlot>().HighlightSlot();
                    slots[14].GetComponent<InventorySlot>().HighlightSlot();
                }
            }
        }
    }

    /// <summary>
    /// This function removes the highlight of all the slots of equipped items and remove their orange borders if any
    /// </summary>
    public void UnHighlightAllSlots()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].GetComponent<InventorySlot>().UnHighlightSlot();
        }
    }

    /// <summary>
    /// This function remove the selection on all the slots and remove the green border if any
    /// </summary>
    public void DeselectAllSlots()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].GetComponent<InventorySlot>().DeselectSlot();
        }
        this.selectedSlot = -1;
    }

    /// <summary>
    /// Whenever a slot is selected and the player select another slot, this functions checks if the item of the previous selected slot
    /// can be moved into the newly selected slot.
    /// This function must be rewritten from scratch to split it in smaller functions, avoid having so many statements, and make it more maintanable
    /// </summary>
    /// <param name="slotId">The id of the targetted slot - 1-8 = unequipped slot, 9 = Left hand, 10 = Right hand, 11 = Armor, 12 = Shoes, 13 = Left ring, 14 = Right ring, 15 = Helmet</param>
    public bool CheckIfSelectedSlotMatchPreviousSelectedSlot(int slotId)
    {
        if (selectedSlot >= 0 && selectedSlot <= 8)
        {
            if (slotId >= 0 && slotId <= 8)
            {
                return true;
            }
            if (DataController.instance.gameData.equipment.inventoryItems[selectedSlot] == 0)
            {
                return false;
            }
            else
            {
                Item itemSelected = listOfItems.GetItem(DataController.instance.gameData.equipment.inventoryItems[selectedSlot]);
                if ((int)itemSelected.itemType == 0 && (slotId == 9 || slotId == 10))
                {
                    if (slotId == 10 && (int)listOfItems.GetItem(DataController.instance.gameData.equipment.frontHandItem).itemType == 1)
                    {
                        StartCoroutine(DisplayTwoHandedWeaponsMessage());
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if ((int)itemSelected.itemType == 1 && slotId == 9)
                {
                    if (DataController.instance.gameData.equipment.backHandItem != 0)
                    {
                        StartCoroutine(DisplayTwoHandedWeaponsMessage());
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if ((int)itemSelected.itemType == 2 && slotId == 11)
                {
                    return true;
                }
                else if ((int)itemSelected.itemType == 3 && slotId == 10)
                {
                    if (DataController.instance.gameData.equipment.frontHandItem != 0 && (int)listOfItems.GetItem(DataController.instance.gameData.equipment.frontHandItem).itemType == 1)
                    {
                        StartCoroutine(DisplayTwoHandedWeaponsMessage());
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else if ((int)itemSelected.itemType == 4 && slotId == 15)
                {
                    return true;
                }
                else if ((int)itemSelected.itemType == 5 && slotId == 12)
                {
                    return true;
                }
                else if ((int)itemSelected.itemType == 6 && (slotId == 13 || slotId == 14))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            if (slotId >= 9 && slotId <= 15)
            {
                return false;
            }
            else if (DataController.instance.gameData.equipment.inventoryItems[slotId] == 0)
            {
                return true;
            }
            Item itemInSlot = listOfItems.GetItem(DataController.instance.gameData.equipment.inventoryItems[slotId]);
            if (selectedSlot == 9 && (int)itemInSlot.itemType == 0)
            {
                return true;
            }
            else if (selectedSlot == 9 && ((int)itemInSlot.itemType == 0))
            {
                return true;
            }
            else if (selectedSlot == 9 && ((int)itemInSlot.itemType == 1))
            {
                if (DataController.instance.gameData.equipment.backHandItem != 0)
                {
                    Debug.Log(selectedSlot);
                    StartCoroutine(DisplayTwoHandedWeaponsMessage());
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (selectedSlot == 10 && ((int)itemInSlot.itemType == 3 || (int)itemInSlot.itemType == 0))
            {
                return true;
            }
            else if (selectedSlot == 11 && (int)itemInSlot.itemType == 2)
            {
                return true;
            }
            else if (selectedSlot == 12 && (int)itemInSlot.itemType == 5)
            {
                return true;
            }
            else if (selectedSlot == 13 && (int)itemInSlot.itemType == 6)
            {
                return true;
            }
            else if (selectedSlot == 14 && (int)itemInSlot.itemType == 6)
            {
                return true;
            }
            else if (selectedSlot == 15 && (int)itemInSlot.itemType == 4)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// Whenever a slot is selected and the player select another slot, this functions move the item from the previously selected slot to the newly selected one, if possible to do so
    /// This function must be rewritten from scratch to split it in smaller functions, avoid having so many statements, and make it more maintanable
    /// </summary>
    /// <param name="slotId">The id of the targetted slot - 1-8 = unequipped slot, 9 = Left hand, 10 = Right hand, 11 = Armor, 12 = Shoes, 13 = Left ring, 14 = Right ring, 15 = Helmet</param>
    public void ChangeItemPosition(int slotId)
    {
        if (selectedSlot >= 0 && selectedSlot <= 8)
        {

            if (slotId >= 0 && slotId <= 8)
            {
                StartCoroutine(SoundController.instance.PlaySE(listOfItems.GetItem(DataController.instance.gameData.equipment.inventoryItems[selectedSlot]).inventorySound, 1));
                int itemTemp = DataController.instance.gameData.equipment.inventoryItems[selectedSlot];
                DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = DataController.instance.gameData.equipment.inventoryItems[slotId];
                DataController.instance.gameData.equipment.inventoryItems[slotId] = itemTemp;
            }
            else
            {

                Item itemSelected = listOfItems.GetItem(DataController.instance.gameData.equipment.inventoryItems[selectedSlot]);
                StartCoroutine(SoundController.instance.PlaySE(itemSelected.inventorySound, 1));
                if ((int)itemSelected.itemType == 0 && slotId == 9)
                {
                    int itemTemp = DataController.instance.gameData.equipment.frontHandItem;
                    DataController.instance.gameData.equipment.frontHandItem = DataController.instance.gameData.equipment.inventoryItems[selectedSlot];
                    DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = itemTemp;
                }
                else if ((int)itemSelected.itemType == 0 && slotId == 10)
                {
                    int itemTemp = DataController.instance.gameData.equipment.backHandItem;
                    DataController.instance.gameData.equipment.backHandItem = DataController.instance.gameData.equipment.inventoryItems[selectedSlot];
                    DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = itemTemp;
                }
                else if ((int)itemSelected.itemType == 1 && slotId == 9)
                {
                    int itemTemp = DataController.instance.gameData.equipment.frontHandItem;
                    DataController.instance.gameData.equipment.frontHandItem = DataController.instance.gameData.equipment.inventoryItems[selectedSlot];
                    DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = itemTemp;
                }
                else if ((int)itemSelected.itemType == 2 && slotId == 11)
                {
                    int itemTemp = DataController.instance.gameData.equipment.armorItem;
                    DataController.instance.gameData.equipment.armorItem = DataController.instance.gameData.equipment.inventoryItems[selectedSlot];
                    DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = itemTemp;
                }
                else if ((int)itemSelected.itemType == 3 && slotId == 10)
                {
                    int itemTemp = DataController.instance.gameData.equipment.backHandItem;
                    DataController.instance.gameData.equipment.backHandItem = DataController.instance.gameData.equipment.inventoryItems[selectedSlot];
                    DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = itemTemp;
                }
                else if ((int)itemSelected.itemType == 4 && slotId == 15)
                {
                    int itemTemp = DataController.instance.gameData.equipment.helmetItem;
                    DataController.instance.gameData.equipment.helmetItem = DataController.instance.gameData.equipment.inventoryItems[selectedSlot];
                    DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = itemTemp;
                }
                else if ((int)itemSelected.itemType == 5 && slotId == 12)
                {
                    int itemTemp = DataController.instance.gameData.equipment.shoesItem;
                    DataController.instance.gameData.equipment.shoesItem = DataController.instance.gameData.equipment.inventoryItems[selectedSlot];
                    DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = itemTemp;
                }
                else if ((int)itemSelected.itemType == 6 && slotId == 13)
                {
                    int itemTemp = DataController.instance.gameData.equipment.leftRing;
                    DataController.instance.gameData.equipment.leftRing = DataController.instance.gameData.equipment.inventoryItems[selectedSlot];
                    DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = itemTemp;
                }
                else if ((int)itemSelected.itemType == 6 && slotId == 14)
                {
                    int itemTemp = DataController.instance.gameData.equipment.rightRing;
                    DataController.instance.gameData.equipment.rightRing = DataController.instance.gameData.equipment.inventoryItems[selectedSlot];
                    DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = itemTemp;
                }
            }
        }
        else
        {
            Item itemInSlot = listOfItems.GetItem(DataController.instance.gameData.equipment.inventoryItems[slotId]);

            if (selectedSlot == 9)
            {
                StartCoroutine(SoundController.instance.PlaySE(listOfItems.GetItem(DataController.instance.gameData.equipment.frontHandItem).inventorySound, 1));
                int itemTemp = DataController.instance.gameData.equipment.frontHandItem;
                DataController.instance.gameData.equipment.frontHandItem = DataController.instance.gameData.equipment.inventoryItems[slotId];
                DataController.instance.gameData.equipment.inventoryItems[slotId] = itemTemp;
            }
            else if (selectedSlot == 10)
            {
                StartCoroutine(SoundController.instance.PlaySE(listOfItems.GetItem(DataController.instance.gameData.equipment.backHandItem).inventorySound, 1));
                int itemTemp = DataController.instance.gameData.equipment.backHandItem;
                DataController.instance.gameData.equipment.backHandItem = DataController.instance.gameData.equipment.inventoryItems[slotId];
                DataController.instance.gameData.equipment.inventoryItems[slotId] = itemTemp;
            }
            else if (selectedSlot == 11)
            {
                SoundController.instance.PlaySE(listOfItems.GetItem(DataController.instance.gameData.equipment.armorItem).inventorySound, 1);
                int itemTemp = DataController.instance.gameData.equipment.armorItem;
                DataController.instance.gameData.equipment.armorItem = DataController.instance.gameData.equipment.inventoryItems[slotId];
                DataController.instance.gameData.equipment.inventoryItems[slotId] = itemTemp;
            }
            else if (selectedSlot == 12)
            {
                StartCoroutine(SoundController.instance.PlaySE(listOfItems.GetItem(DataController.instance.gameData.equipment.shoesItem).inventorySound, 1));
                int itemTemp = DataController.instance.gameData.equipment.shoesItem;
                DataController.instance.gameData.equipment.shoesItem = DataController.instance.gameData.equipment.inventoryItems[slotId];
                DataController.instance.gameData.equipment.inventoryItems[slotId] = itemTemp;
            }
            else if (selectedSlot == 13)
            {
                StartCoroutine(SoundController.instance.PlaySE(listOfItems.GetItem(DataController.instance.gameData.equipment.leftRing).inventorySound, 1));
                int itemTemp = DataController.instance.gameData.equipment.leftRing;
                DataController.instance.gameData.equipment.leftRing = DataController.instance.gameData.equipment.inventoryItems[slotId];
                DataController.instance.gameData.equipment.inventoryItems[slotId] = itemTemp;
            }
            else if (selectedSlot == 14)
            {
                StartCoroutine(SoundController.instance.PlaySE(listOfItems.GetItem(DataController.instance.gameData.equipment.rightRing).inventorySound, 1));
                int itemTemp = DataController.instance.gameData.equipment.rightRing;
                DataController.instance.gameData.equipment.rightRing = DataController.instance.gameData.equipment.inventoryItems[slotId];
                DataController.instance.gameData.equipment.inventoryItems[slotId] = itemTemp;
            }
            else if (selectedSlot == 15)
            {
                StartCoroutine(SoundController.instance.PlaySE(listOfItems.GetItem(DataController.instance.gameData.equipment.helmetItem).inventorySound, 1));
                int itemTemp = DataController.instance.gameData.equipment.helmetItem;
                DataController.instance.gameData.equipment.helmetItem = DataController.instance.gameData.equipment.inventoryItems[slotId];
                DataController.instance.gameData.equipment.inventoryItems[slotId] = itemTemp;
            }
        }
        ReloadInventory();
    }

    private void PlayClickSound()
    {
        StartCoroutine(SoundController.instance.PlaySE(SoundContainerMap.instance.clickSound, 1));
    }

    /// <summary>
    /// This function display a modal panal asking if the user actually wants to throw the selected object.
    /// If yes, then the object is removed from the inventory
    /// </summary>
    public IEnumerator ThrowAwaySelectedObject()
    {
        if (selectedSlot != -1)
        {
            StartCoroutine(UIController.instance.ModalPanelChoice("throw_away_confirm", new string[] { "yes", "no" }));
            yield return new WaitForSeconds(0.1f);
            while (UIController.instance.modalPanelAnswer == null)
            {
                yield return null;
            }
            PlayClickSound();
            string answer = UIController.instance.modalPanelAnswer;
            if (answer == "no")
            {
                DeselectAllSlots();
                UnHighlightAllSlots();
                yield break;
            }
            else
            {
                if (selectedSlot <= 8)
                {
                    DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = 0;
                }
                else
                {
                    switch (selectedSlot)
                    {
                        case 9:
                            DataController.instance.gameData.equipment.frontHandItem = 0;
                            break;
                        case 10:
                            DataController.instance.gameData.equipment.backHandItem = 0;
                            break;
                        case 11:
                            DataController.instance.gameData.equipment.armorItem = 0;
                            break;
                        case 12:
                            DataController.instance.gameData.equipment.shoesItem = 0;
                            break;
                        case 13:
                            DataController.instance.gameData.equipment.leftRing = 0;
                            break;
                        case 14:
                            DataController.instance.gameData.equipment.rightRing = 0;
                            break;
                        case 15:
                            DataController.instance.gameData.equipment.helmetItem = 0;
                            break;
                        default:
                            break;
                    }
                }
                StartCoroutine(SoundController.instance.PlaySE(SoundContainerMap.instance.throwAwaySound, 1));
                ReloadInventory();
            }
        }
        else
        {
            yield return null;
        }
    }

    /// <summary>
    /// Enable the "Throw away" button
    /// </summary>
    public void MakeThrowAwayButtonEnabled()
    {
        throwAwayButton.GetComponent<Button>().interactable = true;
        throwAwayButton.GetComponent<LeanSelectableGraphicColor>().enabled = true;
        throwAwayButton.GetComponent<LeanFingerTap>().enabled = true;
    }

    /// <summary>
    /// Disable the "Throw away" button
    /// </summary>
    public void MakeThrowAwayButtonDisabled()
    {
        throwAwayButton.GetComponent<Button>().interactable = false;
        throwAwayButton.GetComponent<LeanSelectableGraphicColor>().enabled = false;
        throwAwayButton.GetComponent<LeanFingerTap>().enabled = false;
    }

    /// <summary>
    /// Enable the "Use" button
    /// </summary>
    public void MakeUseButtonEnabled()
    {
        useButton.GetComponent<Button>().interactable = true;
        useButton.GetComponent<LeanSelectableGraphicColor>().enabled = true;
        useButton.GetComponent<LeanFingerTap>().enabled = true;
    }

    /// <summary>
    /// Disable the "Use" button
    /// </summary>
    public void MakeUseButtonDisabled()
    {
        useButton.GetComponent<Button>().interactable = false;
        useButton.GetComponent<LeanSelectableGraphicColor>().enabled = false;
        useButton.GetComponent<LeanFingerTap>().enabled = false;
    }

    /// <summary>
    /// This function apply the effects of the selected item, play the sound linked in the item's scriptable object and remove the item from the inventory
    /// </summary>
    public void UseSelectedObject()
    {
        ApplyItemEffects(listOfItems.GetItem(DataController.instance.gameData.equipment.inventoryItems[selectedSlot]));

        StartCoroutine(SoundController.instance.PlaySE(listOfItems.GetItem(DataController.instance.gameData.equipment.inventoryItems[selectedSlot]).useSound, 1));
        DataController.instance.gameData.equipment.inventoryItems[selectedSlot] = 0;
        ReloadInventory();
    }

    /// <summary>
    /// This function apply the effect of an expendable item such as a potion
    /// </summary>
    /// <param name="item">the expendable item having the effects to be applied</param>
    private void ApplyItemEffects(Item item)
    {
        Attributes attributes = DataController.instance.gameData.attributes;
        attributes.strength += item.strengthBonus;
        attributes.dexterity += item.dexterityBonus;
        attributes.intelligence += item.intelligenceBonus;
        attributes.maxHp += item.hpMaxBonus;
        attributes.maxMp += item.mpMaxBonus;
        attributes.hp += item.hpBonus + item.hpMaxBonus;
        if (attributes.hp > attributes.maxHp) { attributes.hp = attributes.maxHp; }
        attributes.mp += item.mpBonus + item.mpMaxBonus;
        if (attributes.mp > attributes.maxMp) { attributes.mp = attributes.maxMp; }
    }

    /// <summary>
    /// This function displays a popup with a warning message when the player tries to equip an item on the left hand and a double handed weapon is equipped
    /// </summary>
    private IEnumerator DisplayTwoHandedWeaponsMessage()
    {
        StartCoroutine(UIController.instance.ModalPanelChoice("double_handed_weapons_warning", new string[] { "ok" }));
        yield return new WaitForSeconds(0.1f);
        while (UIController.instance.modalPanelAnswer == null)
        {
            yield return null;
        }
        PlayClickSound();
        string answer = UIController.instance.modalPanelAnswer;
        if (answer == "ok")
        {
            DeselectAllSlots();
            UnHighlightAllSlots();
            yield break;
        }
    }

    /// <summary>
    /// This function returns the Item being in the inventory on the slot id in parameter
    /// </summary>
    /// <param name="slotId">The id of the slot we want to get the item from - 1-8 = unequipped slot, 9 = Left hand, 10 = Right hand, 11 = Armor, 12 = Shoes, 13 = Left ring, 14 = Right ring, 15 = Helmet</param>
    public Item GetItemInSlot(int slotId)
    {
        if (slotId <= 8 && DataController.instance.gameData.equipment.inventoryItems[slotId] != 0)
        {
            return listOfItems.GetItem(DataController.instance.gameData.equipment.inventoryItems[slotId]);
        }
        if (slotId == 9 && DataController.instance.gameData.equipment.frontHandItem != 0)
        {
            return listOfItems.GetItem(DataController.instance.gameData.equipment.frontHandItem);
        }
        if (slotId == 10 && DataController.instance.gameData.equipment.backHandItem != 0)
        {
            return listOfItems.GetItem(DataController.instance.gameData.equipment.backHandItem);
        }
        if (slotId == 11 && DataController.instance.gameData.equipment.armorItem != 0)
        {
            return listOfItems.GetItem(DataController.instance.gameData.equipment.armorItem);
        }
        if (slotId == 12 && DataController.instance.gameData.equipment.shoesItem != 0)
        {
            return listOfItems.GetItem(DataController.instance.gameData.equipment.shoesItem);
        }
        if (slotId == 13 && DataController.instance.gameData.equipment.leftRing != 0)
        {
            return listOfItems.GetItem(DataController.instance.gameData.equipment.leftRing);
        }
        if (slotId == 14 && DataController.instance.gameData.equipment.rightRing != 0)
        {
            return listOfItems.GetItem(DataController.instance.gameData.equipment.rightRing);
        }
        if (slotId == 15 && DataController.instance.gameData.equipment.helmetItem != 0)
        {
            return listOfItems.GetItem(DataController.instance.gameData.equipment.helmetItem);
        }
        return null;
    }
}
