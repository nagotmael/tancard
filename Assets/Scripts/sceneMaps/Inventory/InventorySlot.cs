﻿using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public int slotId;
    public bool selected = false;
    public bool highlighted = false;

    /// <summary>
    /// Called when tapping on the slot
    /// </summary>
    public void OnInventorySlotTap()
    {
        if (!selected)
        {
            SelectSlot();
        }
        else
        {
            DeselectSlot();
        }
    }

    /// <summary>
    /// Unselect all other selected slots and select the current slot
    /// Display a green border around the slot
    /// </summary>
    public void SelectSlot()
    {
        if (InventoryController.instance.selectedSlot != -1 && InventoryController.instance.CheckIfSelectedSlotMatchPreviousSelectedSlot(this.slotId))
        {
            InventoryController.instance.ChangeItemPosition(slotId);
        }
        else
        {
            if (InventoryController.instance.selectedSlot != -1)
            {
                InventoryController.instance.DeselectSlot(InventoryController.instance.selectedSlot);
            }
            InventoryController.instance.selectedSlot = this.slotId;
            selected = true;
            InventoryController.instance.MakeThrowAwayButtonEnabled();
            if (IsItemUsable())
            {
                InventoryController.instance.MakeUseButtonEnabled();
            }
            InventoryController.instance.UnHighlightAllSlots();
            InventoryController.instance.HighlightCorrespondingSlots();
            Color32 slotColor = this.gameObject.GetComponent<Image>().color;
            slotColor.r = 27;
            slotColor.g = 200;
            slotColor.b = 40;
            this.gameObject.GetComponent<Image>().color = slotColor;
            // Cannot select empty slot so deselect immediately
            if (IsSlotEmpty())
            {
                DeselectSlot();
            }
        }
    }

    /// <summary>
    /// Check if the current slot contains a usable object
    /// </summary>
    private bool IsItemUsable()
    {
        if (IsSlotEmpty())
        {
            return false;
        }
        if (slotId > 8)
        {
            return false;
        }
        else
        {
            return InventoryController.instance.listOfItems.GetItem(DataController.instance.gameData.equipment.inventoryItems[slotId]).isUsableFromInventory;
        }
    }

    /// <summary>
    /// Deselect the current slot and the "throw away" and "Use" buttons. Remove the green border around the slot
    /// </summary>
    public void DeselectSlot()
    {
        InventoryController.instance.MakeThrowAwayButtonDisabled();
        InventoryController.instance.MakeUseButtonDisabled();
        InventoryController.instance.UnHighlightAllSlots();
        InventoryController.instance.selectedSlot = -1;
        selected = false;
        Color32 slotColor = this.gameObject.GetComponent<Image>().color;
        slotColor.r = 147;
        slotColor.g = 147;
        slotColor.b = 147;
        this.gameObject.GetComponent<Image>().color = slotColor;
    }

    /// <summary>
    /// This functions checks if the current slot as an item in it
    /// </summary>
    private bool IsSlotEmpty()
    {
        if ((this.transform.childCount > 2 && this.slotId > 8) || (this.transform.childCount > 1 && this.slotId <= 8))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// This functions adds an orange border around the current slot
    /// </summary>
    public void HighlightSlot()
    {
        highlighted = true;
        Color32 slotColor = this.gameObject.GetComponent<Image>().color;
        slotColor.r = 255;
        slotColor.g = 139;
        slotColor.b = 0;
        this.gameObject.GetComponent<Image>().color = slotColor;
    }

    /// <summary>
    /// This function removed the orange border around the current slot
    /// </summary>
    public void UnHighlightSlot()
    {
        if (highlighted)
        {
            highlighted = false;
            Color32 slotColor = this.gameObject.GetComponent<Image>().color;
            slotColor.r = 147;
            slotColor.g = 147;
            slotColor.b = 147;
            this.gameObject.GetComponent<Image>().color = slotColor;
        }
    }

    /// <summary>
    /// Called when long tapping on the slot.
    /// Open a tooltip showing a description of the item in the slot if any
    /// </summary>
    public void OnInventorySlotLongTapDown()
    {
        if (InventoryController.instance.GetItemInSlot(slotId) == null)
        {
            if (slotId <= 8)
            {
                UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("inventory_empty_slot"));
            }
            else if (slotId == 9)
            {
                UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("inventory_empty_weapon_slot"));
            }
            else if (slotId == 10)
            {
                UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("inventory_empty_weapon_shield_slot"));
            }
            else if (slotId == 11)
            {
                UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("inventory_empty_armor_slot"));
            }
            else if (slotId == 12)
            {
                UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("inventory_empty_shoes_slot"));
            }
            else if (slotId == 13 || slotId == 14)
            {
                UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("inventory_empty_ring_slot"));
            }
            else if (slotId == 15)
            {
                UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue("inventory_empty_helmet_slot"));
            }
        }
        else
        {
            UIController.instance.ShowToolTip(this.transform.position, GetItemDescriptionText(InventoryController.instance.GetItemInSlot(slotId)));
        }
    }

    /// <summary>
    /// This function takes gives a localized description of the item in parameter
    /// It changes color on rare items, shows an item description and shows the item bonuses
    /// </summary>
    /// <param name="item">The item we want to get a description from</param>
    private string GetItemDescriptionText(Item item)
    {
        string description = "";
        if ((int)item.rarity == 0)
        {
            description += "<color=white>";
        }
        else if ((int)item.rarity == 1)
        {
            description += "<color=teal>";
        }
        else if ((int)item.rarity == 2)
        {
            description += "<color=yellow>";
        }
        description += "<size=40>" + LocalizationController.instance.GetLocalizedValue(item.nameKey) + "</size></color>\n";
        switch ((int)item.itemType)
        {
            case 0:
                description += LocalizationController.instance.GetLocalizedValue("weapon");
                break;
            case 1:
                description += LocalizationController.instance.GetLocalizedValue("two_handed_weapon");
                break;
            case 2:
                description += LocalizationController.instance.GetLocalizedValue("armor");
                break;
            case 3:
                description += LocalizationController.instance.GetLocalizedValue("shield");
                break;
            case 4:
                description += LocalizationController.instance.GetLocalizedValue("helmet");
                break;
            case 5:
                description += LocalizationController.instance.GetLocalizedValue("shoes");
                break;
            case 6:
                description += LocalizationController.instance.GetLocalizedValue("ring");
                break;
            case 7:
                description += LocalizationController.instance.GetLocalizedValue("item");
                break;
            default:
                break;
        }
        if (item.hpBonus > 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("hp") + " +" + item.hpBonus;
        }
        if (item.hpBonus < 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("hp") + " " + item.hpBonus;
        }
        if (item.hpMaxBonus > 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("hp_max") + " +" + item.hpMaxBonus;
        }
        if (item.hpMaxBonus < 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("hp_max") + " " + item.hpMaxBonus;
        }
        if (item.mpBonus > 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("mp") + " +" + item.mpBonus;
        }
        if (item.mpBonus < 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("mp") + " " + item.mpBonus;
        }
        if (item.mpMaxBonus > 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("mp_max") + " +" + item.mpMaxBonus;
        }
        if (item.mpMaxBonus < 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("mp_max") + " " + item.mpMaxBonus;
        }
        if (item.strengthBonus > 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("strength") + " +" + item.strengthBonus;
        }
        if (item.strengthBonus < 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("strength") + " " + item.strengthBonus;
        }
        if (item.dexterityBonus > 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("dexterity") + " +" + item.dexterityBonus;
        }
        if (item.dexterityBonus < 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("dexterity") + " " + item.dexterityBonus;
        }
        if (item.intelligenceBonus > 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("intelligence") + " +" + item.intelligenceBonus;
        }
        if (item.intelligenceBonus < 0)
        {
            description += "\n" + LocalizationController.instance.GetLocalizedValue("intelligence") + " " + item.intelligenceBonus;
        }
        return description;
    }

    /// <summary>
    /// Called after stopping long tapping on the slot. Hide the tooltip
    /// </summary>
    public void OnInventorySlotLongTapUp()
    {
        UIController.instance.HideToolTip();
    }

}
