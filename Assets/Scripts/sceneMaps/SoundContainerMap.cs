﻿using UnityEngine;

public class SoundContainerMap : MonoBehaviour
{
    public static SoundContainerMap instance;
    public AudioClip clickSound;
    public AudioClip throwAwaySound;

    void Start()
    {
        instance = this;
    }
}
