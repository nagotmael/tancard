﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

public class MapItemInteraction : MonoBehaviour
{
    public float shakingStartAngle;
    public string messageKey;
    public Encounter encounter;
    private Sequence shakeObjectSequence;
    private bool isActive = false;

    public void OnLongTapDown()
    {
        UIController.instance.ShowToolTip(this.transform.position, LocalizationController.instance.GetLocalizedValue(messageKey));
    }

    public void OnLongTapUp()
    {
        UIController.instance.HideToolTip();
    }

    // TBC. Now pointing to battle scene for prototyping purpose
    public void OnTap()
    {
        if (isActive)
        {
            StartCoroutine(StartBattle());
        }
    }

    private IEnumerator StartBattle()
    {
        DataController.instance.currentEncounter = encounter;
        DataController.instance.SaveGameData();
        yield return LoadingScreenController.instance.LoadScene("Battlefield", true);
        GameObject.Find("RootObjectMap").SetActive(false);
    }

    /// <summary>
    /// This method makes a map item game object shaking so that the player understands he can interact with it.
    /// </summary>
    public IEnumerator ShakeObject()
    {
        isActive = true;
        GameObject childObjectToShake = this.transform.GetChild(0).gameObject;
        childObjectToShake.transform.Rotate(new Vector3(0, 0, shakingStartAngle));
        yield return null;
        shakeObjectSequence = DOTween.Sequence();
        shakeObjectSequence
                .Append(childObjectToShake.transform.DORotate(new Vector3(0, 0, shakingStartAngle - 60), 1, RotateMode.Fast))
                .SetLoops(-1, LoopType.Yoyo);
        yield return null;
    }

    /// <summary>
    /// This method make a shaking map item game object stopping to shake
    /// </summary>
    public void StopShakingObject()
    {
        isActive = false;
        shakeObjectSequence.Kill();
        GameObject childObjectToShake = this.transform.GetChild(0).gameObject;
        childObjectToShake.transform.Rotate(0, 0, -childObjectToShake.transform.localEulerAngles.z);
    }
}
