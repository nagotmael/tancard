﻿using System.Collections;
using UnityEngine;

public class MapScene : MonoBehaviour
{
    public Map map;
    public ListOfMaps listOfMaps;
    [SerializeField]
    private GameObject linePrefab;
    [SerializeField]
    private GameObject canvasObject;
    [SerializeField]
    private GameObject rootObject;
    private MapController mapController;
    public GameObject settingsButton;
    public GameObject inventoryButton;
    public GameObject inventory;

    IEnumerator Start()
    {
        string mapName = DataController.instance.GetCurrentMap();
        map = listOfMaps.GetMap(mapName);
        InitializeMapController();
        LoadMapSound(map);
        mapController.LoadMapBackground();
        mapController.LoadMapGameData();
        mapController.InitializeButtons(settingsButton, inventoryButton, inventory);
        if (mapController.mapTilesId == null || mapController.mapItemLinks == null)
        {
            mapController.GenerateMap();
            mapController.GenerateMapItemLinks();
        }
        yield return StartCoroutine(mapController.DisplayMap());
        mapController.DisplayMapItemLinks();
        mapController.AddMapDataToGameData();
        DataController.instance.SaveGameData();
        ShakeObject(1, 1);
    }

    private void InitializeMapController()
    {
        mapController = new MapController();
        mapController.map = map;
        mapController.linePrefab = linePrefab;
        mapController.canvasObject = canvasObject;
        mapController.rootObject = rootObject;
    }

    /// <summary>
    /// This method plays the background music and background sound corresponding to the map.
    /// </summary>
    /// <param name="Map map">The map from which the bgm and bgs have to be loaded.</param>
    private void LoadMapSound(Map map)
    {
        StartCoroutine(SoundController.instance.PlayBackgroundMusic(map.bgmFileMap, 1, 1));
        StartCoroutine(SoundController.instance.PlayBackgroundSound(map.bgsFileMap, 1, 1));
    }

    /// <summary>
    /// This method makes a map item game object shaking so that the player understands he can interact with it.
    /// </summary>
    /// <param name="int x">x of the map item on the map</param>
    /// <param name="int y">y of the map item on the map</param>
    private void ShakeObject(int x, int y)
    {
        GameObject objectToAnimate = mapController.GetMapItemFromCoordinates(x, y);
        StartCoroutine(objectToAnimate.GetComponent<MapItemInteraction>().ShakeObject());
    }

    /// <summary>
    /// This method make a shaking map item game object stopping to shake
    /// </summary>
    /// <param name="int x">x of the map item on the map</param>
    /// <param name="int y">y of the map item on the map</param>
    private void StopShakingObject(int x, int y)
    {
        GameObject objectToAnimate = mapController.GetMapItemFromCoordinates(x, y);
        objectToAnimate.GetComponent<MapItemInteraction>().StopShakingObject();
    }
}

public static class Pivot
{
    /// <summary>
    /// Set pivot without changing the position of the element
    /// </summary>
    public static void SetPivot(this RectTransform rectTransform, Vector2 pivot)
    {
        Vector3 deltaPosition = rectTransform.pivot - pivot;    // get change in pivot
        deltaPosition.Scale(rectTransform.rect.size);           // apply sizing
        deltaPosition.Scale(rectTransform.localScale);          // apply scaling
        deltaPosition = rectTransform.rotation * deltaPosition; // apply rotation

        rectTransform.pivot = pivot;                            // change the pivot
        rectTransform.localPosition -= deltaPosition;           // reverse the position change
    }
}

public class MapItemLink
{
    public int[] origin;
    public int[] destination;
    public MapItemLink(int tile1X, int tile1Y, int tile2X, int tile2Y)
    {
        origin = new int[2] { tile1X, tile1Y };
        destination = new int[2] { tile2X, tile2Y };
    }
}
