﻿using System.Collections.Generic;

public class GameData
{
    public string localizationLanguage;
    public bool isGameSessionStarted = false;
    public int level;
    public string currentMap;
    public int[,] mapTiles;
    public List<MapItemLink> mapItemLinks;
    public List<int> allies;
    public Equipment equipment = new Equipment();
    public int gold;
    public Attributes attributes = new Attributes();

}

public class Equipment
{
    public int[] inventoryItems = new int[9];
    public int helmetItem;
    public int armorItem;
    public int frontHandItem;
    public int backHandItem;
    public int shoesItem;
    public int leftRing;
    public int rightRing;
}

public class Attributes
{
    public int strength;
    public int dexterity;
    public int intelligence;
    public int maxHp;
    public int maxMp;
    public int hp;
    public int mp;
}
