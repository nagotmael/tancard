﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;

public class DataController : MonoBehaviour
{

    public static DataController instance;
    public GameData gameData;
    private bool isReady = false;
    private string gameDataFileName = "data.json";
    public string filePath;
    // Used to make the transition between the map and the battle. Not in the gameData since it does not have to be saved.
    public Encounter currentEncounter;
    public ListOfItems listOfItems;

    /*
    Use Singleton pattern for the data controller object to have it never destroyed and only once instance of it.
    It needs to be present in all the scenes.
     */
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        filePath = Path.Combine(Application.persistentDataPath, gameDataFileName);
        LoadGameData();
    }

    /// <summary>
    /// Load the game data from the save file. If no save file exists, then it creates one.
    /// </summary>
    public void LoadGameData()
    {
        /* 
        Cannot just read the files in the streaming path on Android since it is stored in an apk.
        Need a www reader for that specific case.
        */
        string dataAsJson = null;
        if (File.Exists(filePath))
        {
            dataAsJson = File.ReadAllText(filePath);
            gameData = JsonConvert.DeserializeObject<GameData>(dataAsJson);
        }
        else
        {
            gameData = new GameData();
            SaveGameData();
        }
        isReady = true;
    }

    /// <summary>
    /// Save the game data.
    /// </summary>
    public void SaveGameData()
    {
        string dataAsJson = JsonConvert.SerializeObject(gameData, Formatting.Indented);

        File.WriteAllText(filePath, dataAsJson);
    }

    /// <summary>
    /// Return true when the save file is fully loaded.
    /// This function is done to avoid trying to access data when they are not fully loaded.
    /// </summary>
    public bool GetIsReady()
    {
        return isReady;
    }

    /// <summary>
    /// Return true when a game is ongoing so that the main menu can allow to continue the game.
    /// </summary>
    public bool IsGameSessionStarted()
    {
        return gameData.isGameSessionStarted;
    }

    public int GetLevel()
    {
        return gameData.level;
    }

    public string GetCurrentMap()
    {
        return gameData.currentMap;
    }

    /// <summary>
    /// Return the list of items equipped by the hero
    /// </summary>
    public List<Item> GetEquipedItems()
    {
        List<Item> equipedItems = new List<Item>();
        if (gameData.equipment.frontHandItem != 0)
        {
            equipedItems.Add(listOfItems.GetItem(gameData.equipment.frontHandItem));
        }
        if (gameData.equipment.backHandItem != 0)
        {
            equipedItems.Add(listOfItems.GetItem(gameData.equipment.backHandItem));
        }
        if (gameData.equipment.armorItem != 0)
        {
            equipedItems.Add(listOfItems.GetItem(gameData.equipment.armorItem));
        }
        if (gameData.equipment.shoesItem != 0)
        {
            equipedItems.Add(listOfItems.GetItem(gameData.equipment.shoesItem));
        }
        if (gameData.equipment.leftRing != 0)
        {
            equipedItems.Add(listOfItems.GetItem(gameData.equipment.leftRing));
        }
        if (gameData.equipment.rightRing != 0)
        {
            equipedItems.Add(listOfItems.GetItem(gameData.equipment.rightRing));
        }
        if (gameData.equipment.helmetItem != 0)
        {
            equipedItems.Add(listOfItems.GetItem(gameData.equipment.helmetItem));
        }
        return equipedItems;
    }

    /// <summary>
    /// Return the strength bonus based on the bonuses of all equipped items
    /// </summary>
    public int GetStrengthBonus()
    {
        int strengthBonus = 0;
        List<Item> equipedItems = GetEquipedItems();
        for (int i = 0; i < equipedItems.Count; i++)
        {
            strengthBonus += equipedItems[i].strengthBonus;
        }
        return strengthBonus;
    }

    /// <summary>
    /// Return the dexterity bonus based on the bonuses of all equipped items 
    /// </summary>
    public int GetDexterityBonus()
    {
        int dexterityBonus = 0;
        List<Item> equipedItems = GetEquipedItems();
        for (int i = 0; i < equipedItems.Count; i++)
        {
            dexterityBonus += equipedItems[i].dexterityBonus;
        }
        return dexterityBonus;
    }

    /// <summary>
    /// Return the intelligence bonus based on the bonuses of all equipped items 
    /// </summary>

    public int GetIntelligenceBonus()
    {
        int intelligenceBonus = 0;
        List<Item> equipedItems = GetEquipedItems();
        for (int i = 0; i < equipedItems.Count; i++)
        {
            intelligenceBonus += equipedItems[i].intelligenceBonus;
        }
        return intelligenceBonus;
    }

    /// <summary>
    /// Return the Max hp bonus based on the bonuses of all equipped items 
    /// </summary>

    public int GetMaxHpBonus()
    {
        int hpMaxBonus = 0;
        List<Item> equipedItems = GetEquipedItems();
        for (int i = 0; i < equipedItems.Count; i++)
        {
            hpMaxBonus += equipedItems[i].hpMaxBonus;
        }
        return hpMaxBonus;
    }

    /// <summary>
    /// Return the Max mp bonus based on the bonuses of all equipped items 
    /// </summary>

    public int GetMaxMpBonus()
    {
        int mpMaxBonus = 0;
        List<Item> equipedItems = GetEquipedItems();
        for (int i = 0; i < equipedItems.Count; i++)
        {
            mpMaxBonus += equipedItems[i].mpMaxBonus;
        }
        return mpMaxBonus;
    }
}
