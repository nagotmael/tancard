﻿using UnityEngine;

public class TestController : MonoBehaviour
{
    public static TestController instance;
    public ListOfMaps listOfMaps;
    public GameObject dataController;
    public GameObject loadingScreenController;
    public GameObject localizationController;
    public GameObject soundController;
    public GameObject uiController;
    public GameObject tooltip;
    public GameObject modalPanel;
    public GameObject genericButton;
    public AudioClip bgmForest;
    public AudioClip bgmIntro;
    public AudioClip bgmDungeon;
    public AudioClip bgsForest;
    public AudioClip bgsDungeon;
    public Map testMap;
    public GameObject linePrefab;
    public GameObject battleControllerPrefab;
    public GameObject heroGO;

    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
}
