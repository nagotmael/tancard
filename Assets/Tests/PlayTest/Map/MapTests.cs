﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

namespace Tests
{
    public class MapTests
    {
        [UnityTest]
        public IEnumerator TestGetMapFunction()
        {
            Debug.Log("This test checks that the GetMap function gets the correct map objet from a string");
            yield return null;
            ListOfMaps listOfMaps = TestController.instance.listOfMaps;
            Assert.AreEqual(listOfMaps.GetMap("Forest").bgmFileMap.name, "BgmForest");
        }

        [UnityTest]
        public IEnumerator TestLoadBackGroundFunction()
        {
            Debug.Log("This test checks that the load background function actually loads the background");
            yield return null;
            MapController mapController = new MapController();
            mapController.map = TestController.instance.testMap;
            GameObject rootObject = new GameObject();
            mapController.rootObject = rootObject;
            mapController.LoadMapBackground();
            yield return null;
            Assert.IsNotNull(GameObject.Find(mapController.map.mapBackground.name + "(Clone)"));
        }

        [UnityTest]
        public IEnumerator TestLoadMapDataFunction()
        {
            Debug.Log("This test that some map items and map links are loaded by the loadMapData function");
            yield return null;
            string testDataPath = "Assets/Tests/TestsData/DataController/data.json";
            GameObject.Instantiate(TestController.instance.dataController as GameObject);
            yield return null;
            DataController.instance.filePath = testDataPath;
            DataController.instance.LoadGameData();
            while (!DataController.instance.GetIsReady())
            {
                yield return null;
            }

            MapController mapController = new MapController();
            mapController.LoadMapGameData();
            yield return null;
            Assert.IsNotNull(mapController.mapItemLinks);
            Assert.IsNotNull(mapController.mapTilesId);
        }

        [UnityTest]
        public IEnumerator TestGenerateMapGeneratesOnlyOneTileOnMapBorders()
        {
            Debug.Log("This test checks that on the map border, only one tile is generated");
            yield return null;
            MapController mapController = new MapController();
            mapController.map = TestController.instance.testMap;
            mapController.GenerateMap();
            yield return null;
            Assert.AreEqual(mapController.mapTilesId[0, 1], -1);
            Assert.AreEqual(mapController.mapTilesId[mapController.map.sizeX - 1, 1], -1);
        }

        [UnityTest]
        public IEnumerator TestGenerateMapGeneratesTheRightNumberOfEachTiles()
        {
            Debug.Log("This test checks if the right number of tiles of each type is generated based on map configuration");
            yield return null;
            MapController mapController = new MapController();
            mapController.map = TestController.instance.testMap;
            mapController.GenerateMap();
            yield return null;
            Assert.AreEqual(mapController.CountNumberOfTilesOfId(3), 0);
            Assert.AreEqual(mapController.CountNumberOfTilesOfId(2), 1);
            Assert.IsTrue(mapController.CountNumberOfTilesOfId(1) <= 8);
            Assert.IsTrue(mapController.CountNumberOfTilesOfId(1) >= 5);
        }

        [UnityTest]
        public IEnumerator TestGenerateMapGeneratesTilesAtTheXRangeDefinedInTheConfiguration()
        {
            Debug.Log("This test checks the tiles are generated at the X range defined in the configuration");
            yield return null;
            MapController mapController = new MapController();
            mapController.map = TestController.instance.testMap;
            mapController.GenerateMap();
            yield return null;
            int tileIdToCheck = 2;
            int x = -1;
            for (int i = 0; i < mapController.mapTilesId.GetLength(0); i++)
            {
                for (int j = 0; j < mapController.mapTilesId.GetLength(1); j++)
                {
                    if (mapController.mapTilesId[i, j] == tileIdToCheck)
                    {
                        x = i + 1;
                    }
                }
            }
            Assert.IsTrue(x <= 5);
            Assert.IsTrue(x >= 4);
        }

        [UnityTest]
        public IEnumerator TestDisplayMapInstantiatesMapTilesObjects()
        {
            Debug.Log("This test checks the DisplayMap function instantiates the Map tiles object");
            yield return null;
            MapController mapController = new MapController();
            mapController.map = TestController.instance.testMap;
            mapController.GenerateMap();
            mapController.canvasObject = createCanvas();
            yield return null;
            yield return mapController.DisplayMap();
            int nbTilesInstantiated = 0;
            foreach (GameObject go in Object.FindObjectsOfType<GameObject>())
            {
                Debug.Log(go.name);
                if (go.name.Contains("mapItem"))
                {
                    nbTilesInstantiated++;
                }
            }
            Assert.AreEqual(nbTilesInstantiated, mapController.map.sizeX * mapController.map.sizeY - (mapController.map.sizeY - 1) * 2);
        }

        [UnityTest]
        public IEnumerator TestGenerateLinksNumberWhenTilesConnectionRateIsZero()
        {
            Debug.Log("This test checks that the links are generated only between objects of the same Y when Tiles Connection Rate is set to Zero");
            yield return null;
            MapController mapController = new MapController();
            mapController.map = TestController.instance.testMap;
            mapController.map.tilesConnectionRatio = 0;
            mapController.GenerateMap();
            yield return null;
            mapController.GenerateMapItemLinks();
            yield return null;
            Assert.AreEqual(mapController.mapItemLinks.Count, (mapController.map.sizeX - 1) * mapController.map.sizeY);
        }

        [UnityTest]
        [TestCase(7, 3, 34, ExpectedResult = null)]
        [TestCase(9, 4, 68, ExpectedResult = null)]
        [TestCase(7, 1, 6, ExpectedResult = null)]
        public IEnumerator TestGenerateLinksNumberWhenTilesConnectionRateIsHundredAndAllowTileCrossingEachOther(int mapSizeX, int mapSizeY, int ExpectedTilesConnections)
        {
            Debug.Log("This test checks when the Tiles Connection Rate is 100 and the tiles connections can cross each other");
            yield return null;
            MapController mapController = new MapController();
            mapController.map = TestController.instance.testMap;
            mapController.map.tilesConnectionRatio = 100;
            mapController.map.allowTileConnectionCrossingEachOther = true;
            mapController.map.sizeX = mapSizeX;
            mapController.map.sizeY = mapSizeY;
            mapController.GenerateMap();
            yield return null;
            mapController.GenerateMapItemLinks();
            yield return null;
            Assert.AreEqual(mapController.mapItemLinks.Count, ExpectedTilesConnections);
        }

        [UnityTest]
        [TestCase(7, 3, 26, ExpectedResult = null)]
        [TestCase(9, 4, 50, ExpectedResult = null)]
        [TestCase(7, 1, 6, ExpectedResult = null)]
        public IEnumerator TestGenerateLinksNumberWhenTilesConnectionRateIsHundredAndNotAllowTileCrossingEachOther(int mapSizeX, int mapSizeY, int ExpectedTilesConnections)
        {
            Debug.Log("This test checks when the Tiles Connection Rate is 100 and the tiles connections cannot cross each other");
            yield return null;
            MapController mapController = new MapController();
            mapController.map = TestController.instance.testMap;
            mapController.map.tilesConnectionRatio = 100;
            mapController.map.allowTileConnectionCrossingEachOther = false;
            mapController.map.sizeX = mapSizeX;
            mapController.map.sizeY = mapSizeY;
            mapController.GenerateMap();
            yield return null;
            mapController.GenerateMapItemLinks();
            yield return null;
            Assert.AreEqual(mapController.mapItemLinks.Count, ExpectedTilesConnections);
        }

        [UnityTest]
        [TestCase(7, 3, 26, ExpectedResult = null)]
        [TestCase(9, 4, 50, ExpectedResult = null)]
        [TestCase(7, 1, 6, ExpectedResult = null)]
        public IEnumerator TestDisplayMapItemLinksInstantiatesMapTilesConnections(int mapSizeX, int mapSizeY, int ExpectedTilesConnections)
        {

            Debug.Log("This test checks the the DisplayMapItemLinks function instantiates the link objects between Map tiles");
            yield return null;
            MapController mapController = new MapController();
            mapController.map = TestController.instance.testMap;
            mapController.linePrefab = TestController.instance.linePrefab;
            mapController.map.sizeX = mapSizeX;
            mapController.map.sizeY = mapSizeY;
            mapController.map.tilesConnectionRatio = 100;
            mapController.map.allowTileConnectionCrossingEachOther = false;
            mapController.GenerateMap();
            mapController.GenerateMapItemLinks();
            mapController.canvasObject = createCanvas();
            yield return null;
            yield return mapController.DisplayMap();
            mapController.DisplayMapItemLinks();
            yield return null;
            int nbLinksInstantiated = 0;
            foreach (GameObject go in Object.FindObjectsOfType<GameObject>())
            {
                if (go.name.Contains("Line"))
                {
                    nbLinksInstantiated++;
                }
            }
            Assert.AreEqual(nbLinksInstantiated, ExpectedTilesConnections);
        }

        [SetUp]
        public void LoadTestScene()
        {
            if (SceneManager.GetActiveScene().name != "testScene")
            {
                SceneManager.LoadScene("testScene", LoadSceneMode.Single);
            }
        }

        [TearDown]
        public void UnloadTestScene()
        {
            if (SceneManager.GetActiveScene().name != "testScene")
            {
                SceneManager.UnloadSceneAsync("testScene");
            }
        }

        [TearDown]
        public void DestroyAllGameObjects()
        {
            foreach (GameObject go in Object.FindObjectsOfType<GameObject>())
            {
                Object.Destroy(go);
            }
        }

        private GameObject createCanvas()
        {
            GameObject canvasObject = new GameObject();
            canvasObject.name = "Canvas";
            canvasObject.AddComponent<Canvas>();
            canvasObject.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            return canvasObject;
        }

    }
}
