﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace Tests
{
    public class BattleGridTests
    {

        [UnityTest]
        [TestCase(10, 4, new string[] { "Hex 1-4", "Hex 2-4", "Hex 3-4", "Hex 4-4", "Hex 5-4", "Hex 6-4", "Hex 7-4", "Hex 8-4", "Hex 9-4", "Hex 10-4", "Hex 2-3", "Hex 3-3", "Hex 4-3", "Hex 5-3", "Hex 6-3", "Hex 7-3", "Hex 8-3", "Hex 9-3", "Hex 10-3", "Hex 11-3", "Hex 2-2", "Hex 3-2", "Hex 4-2", "Hex 5-2", "Hex 6-2", "Hex 7-2", "Hex 8-2", "Hex 9-2", "Hex 10-2", "Hex 11-2", "Hex 3-1", "Hex 4-1", "Hex 5-1", "Hex 6-1", "Hex 7-1", "Hex 8-1", "Hex 9-1", "Hex 10-1", "Hex 11-1", "Hex 12-1" }, ExpectedResult = null)]
        [TestCase(7, 2, new string[] { "Hex 1-2", "Hex 2-2", "Hex 3-2", "Hex 4-2", "Hex 5-2", "Hex 6-2", "Hex 7-2", "Hex 2-1", "Hex 3-1", "Hex 4-1", "Hex 5-1", "Hex 6-1", "Hex 7-1", "Hex 8-1" }, ExpectedResult = null)]
        public IEnumerator TestCreateGridFunction(int battlefieldSizeX, int battlefieldSizeY, string[] hexNameList)
        {
            Debug.Log("This test checks than the grid creation function generates the expected hexagons");
            TestController.instance.testMap.battlefieldSizeX = battlefieldSizeX;
            TestController.instance.testMap.battlefieldSizeY = battlefieldSizeY;
            CreateTheGrid();
            yield return null;
            for (int i = 0; i < hexNameList.Length; i++)
            {
                Assert.IsNotNull(GameObject.Find(hexNameList[i]));
            }
        }

        [UnityTest]
        public IEnumerator TestGenerateObstaclesFunction()
        {
            Debug.Log("This test checks that the generate obstacles function generate obstacles at the expected position without overlap");
            CreateTheGrid();
            yield return null;
            System.Array.Resize(ref GridController.instance.map.possibleObstacles, 4);
            GridController.instance.map.possibleObstacles[1] = GridController.instance.map.possibleObstacles[0];
            GridController.instance.map.possibleObstacles[2] = GridController.instance.map.possibleObstacles[0];
            GridController.instance.map.possibleObstacles[3] = GridController.instance.map.possibleObstacles[0];
            GridController.instance.GenerateObstacles();
            Assert.IsTrue(GridController.instance.CheckIfObstacleAtPosition(3, 1));
            Assert.IsTrue(GridController.instance.CheckIfObstacleAtPosition(4, 1));
            Assert.IsTrue(GridController.instance.CheckIfObstacleAtPosition(3, 2));
            Assert.IsTrue(GridController.instance.CheckIfObstacleAtPosition(4, 2));
        }

        [UnityTest]
        [TestCase(3, 1, -4.55f, 0.7225f, ExpectedResult = null)]
        [TestCase(5, 4, -0.97f, 3.355f, ExpectedResult = null)]
        public IEnumerator TestDisplayObstaclesFunction(int obstacleX, int obstacleY, float obstacleWorldX, float obstacleWorldY)
        {
            Debug.Log("This test checks that the obstacles are displayed at the right place after being generated");
            CreateTheGrid();
            yield return null;
            System.Array.Resize(ref GridController.instance.map.possibleObstacles, 1);
            GridController.instance.GenerateObstacles();
            GridController.instance.obstaclesList[0].obstaclePosition = new Vector2Int(obstacleX, obstacleY);
            GridController.instance.DisplayObstacles();
            yield return null;
            GameObject obstacleGO = GameObject.Find(GridController.instance.map.possibleObstacles[0].obstaclePrefab.name + "(Clone)");
            Assert.IsNotNull(obstacleGO);
            Assert.IsTrue(Mathf.Approximately(obstacleGO.transform.position.x, obstacleWorldX));
            Assert.IsTrue(Mathf.Approximately(obstacleGO.transform.position.y, obstacleWorldY));
            Assert.AreEqual(obstacleGO.GetComponent<SpriteRenderer>().sortingOrder, 50 - obstacleY);
        }

        [UnityTest]
        [TestCase(3, 4, ExpectedResult = null)]
        [TestCase(3, 1, ExpectedResult = null)]
        public IEnumerator TestHighlightTile(int x, int y)
        {
            Debug.Log("This test checks that the highlight tile function highlight the hexagon");
            CreateTheGrid();
            yield return null;
            GridController.instance.HighlightTile(new Vector2Int(x, y));
            Debug.Log(GridController.instance.GetHexagon(x, y).name);
            Assert.IsTrue(GridController.instance.GetHexagon(x, y).GetComponent<HexInteractions>().highlighted);
        }

        [UnityTest]
        public IEnumerator TestRemoveHighlightOnAllTiles()
        {
            Debug.Log("This test checks that the RemoveHightlightAllTiles function remove highlight on all tiles");
            CreateTheGrid();
            yield return null;
            GridController.instance.HighlightTile(new Vector2Int(2, 3));
            GridController.instance.HighlightTile(new Vector2Int(2, 2));
            GridController.instance.RemoveHightlightAllTiles();
            Assert.IsFalse(GridController.instance.GetHexagon(2, 2).GetComponent<HexInteractions>().highlighted);
            Assert.IsFalse(GridController.instance.GetHexagon(2, 3).GetComponent<HexInteractions>().highlighted);
            Assert.Zero(GridController.instance.GetHexagon(2, 2).transform.GetChild(0).GetComponent<SpriteRenderer>().color.a);
            Assert.Zero(GridController.instance.GetHexagon(2, 3).transform.GetChild(0).GetComponent<SpriteRenderer>().color.a);
        }

        [UnityTest]
        public IEnumerator TestGetEmptyNeighboursNoObstacle()
        {
            Debug.Log("This test checks that the Get Empty Neighbours function returns the six hexagons when no obstacle in the way");
            CreateTheGrid();
            yield return null;
            System.Array.Resize(ref GridController.instance.map.possibleObstacles, 1);
            GridController.instance.GenerateObstacles();
            GridController.instance.obstaclesList[0].obstaclePosition = new Vector2Int(2, 2);
            GridController.instance.DisplayObstacles();
            List<Vector2Int> neighbours = GridController.instance.GetEmptyNeighbours(new Vector2Int(5, 3));
            Assert.IsTrue(neighbours.Contains(new Vector2Int(4, 3)));
            Assert.IsTrue(neighbours.Contains(new Vector2Int(6, 3)));
            Assert.IsTrue(neighbours.Contains(new Vector2Int(5, 4)));
            Assert.IsTrue(neighbours.Contains(new Vector2Int(4, 4)));
            Assert.IsTrue(neighbours.Contains(new Vector2Int(5, 2)));
            Assert.IsTrue(neighbours.Contains(new Vector2Int(6, 2)));
            yield return null;
        }

        [UnityTest]
        public IEnumerator TestGetEmptyNeighboursWithObstacle()
        {
            Debug.Log("This test checks that the Get Empty Neighbours function returns only five hexagons when there is one obstacle in the way");
            CreateTheGrid();
            yield return null;
            System.Array.Resize(ref GridController.instance.map.possibleObstacles, 1);
            GridController.instance.GenerateObstacles();
            GridController.instance.obstaclesList[0].obstaclePosition = new Vector2Int(6, 2);
            GridController.instance.DisplayObstacles();
            List<Vector2Int> neighbours = GridController.instance.GetEmptyNeighbours(new Vector2Int(5, 3));
            Assert.IsTrue(neighbours.Contains(new Vector2Int(4, 3)));
            Assert.IsTrue(neighbours.Contains(new Vector2Int(6, 3)));
            Assert.IsTrue(neighbours.Contains(new Vector2Int(5, 4)));
            Assert.IsTrue(neighbours.Contains(new Vector2Int(4, 4)));
            Assert.IsTrue(neighbours.Contains(new Vector2Int(5, 2)));
            Assert.IsFalse(neighbours.Contains(new Vector2Int(6, 2)));
        }

        [UnityTest]
        public IEnumerator TestGetEmptyHexInRadius()
        {
            Debug.Log("This test checks that the GetEmptyHexInRadius function returns the hex without obstacles");
            CreateTheGrid();
            yield return null;
            System.Array.Resize(ref GridController.instance.map.possibleObstacles, 1);
            GridController.instance.GenerateObstacles();
            GridController.instance.obstaclesList[0].obstaclePosition = new Vector2Int(6, 2);
            GridController.instance.DisplayObstacles();
            // GridController.instance.encounter = GridController.instance.map.possibleEncounters[0].encounter;
            // GridController.instance.InitializeActorsPosition();
            List<Vector2Int> hexInRadius = GridController.instance.GetEmptyHexInRadius(new Vector2Int(4, 2), 3);
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(3, 3)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(3, 2)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(4, 3)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(5, 2)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(4, 1)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(5, 1)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(2, 2)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(2, 3)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(3, 1)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(2, 4)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(3, 4)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(4, 4)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(5, 3)));
            Assert.IsFalse(hexInRadius.Contains(new Vector2Int(6, 2)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(6, 1)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(1, 4)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(5, 4)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(6, 3)));
            Assert.IsTrue(hexInRadius.Contains(new Vector2Int(7, 1)));
            Assert.IsFalse(hexInRadius.Contains(new Vector2Int(8, 1)));
            Assert.IsFalse(hexInRadius.Contains(new Vector2Int(4, 2)));
        }

        [UnityTest]
        public IEnumerator TestHighlightEmptyHexInRadius()
        {
            Debug.Log("This test checks that the HighlightEmptyHexInRadius function highlight the empty hex in the radius");
            CreateTheGrid();
            yield return null;
            System.Array.Resize(ref GridController.instance.map.possibleObstacles, 1);
            GridController.instance.GenerateObstacles();
            GridController.instance.obstaclesList[0].obstaclePosition = new Vector2Int(6, 2);
            GridController.instance.DisplayObstacles();
            GridController.instance.HighlightEmptyHexInRadius(new Vector2Int(4, 2), 3);
            Assert.IsTrue(GridController.instance.GetHexagon(3, 3).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(3, 2).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(4, 3).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(5, 2).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(4, 1).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(5, 1).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(2, 2).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(2, 3).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(3, 1).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(2, 4).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(3, 4).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(4, 4).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(5, 3).GetComponent<HexInteractions>().highlighted);
            Assert.IsFalse(GridController.instance.GetHexagon(6, 2).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(6, 1).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(1, 4).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(5, 4).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(6, 3).GetComponent<HexInteractions>().highlighted);
            Assert.IsTrue(GridController.instance.GetHexagon(7, 1).GetComponent<HexInteractions>().highlighted);
            Assert.IsFalse(GridController.instance.GetHexagon(8, 1).GetComponent<HexInteractions>().highlighted);
            Assert.IsFalse(GridController.instance.GetHexagon(4, 2).GetComponent<HexInteractions>().highlighted);
        }

        [UnityTest]
        public IEnumerator TestGetHexPath()
        {
            Debug.Log("This test checks that the GetHexPath function that generates a A* path returns the correct path (Tested for a path having only one resolution possible)");
            CreateTheGrid();
            yield return null;
            System.Array.Resize(ref GridController.instance.map.possibleObstacles, 2);
            GridController.instance.map.possibleObstacles[1] = GridController.instance.map.possibleObstacles[0];
            GridController.instance.GenerateObstacles();
            GridController.instance.obstaclesList[0].obstaclePosition = new Vector2Int(2, 3);
            GridController.instance.obstaclesList[1].obstaclePosition = new Vector2Int(3, 2);
            GridController.instance.DisplayObstacles();
            List<HexPath> hexPathList = GridController.instance.GetHexPath(new Vector2Int(2, 2), new Vector2Int(1, 4));
            string debugString = "";
            for (int i = 0; i < hexPathList.Count; i++)
            {
                debugString += "[" + hexPathList[i].originPath.x + "," + hexPathList[i].originPath.y + "-" + hexPathList[i].destPath.x + "," + hexPathList[i].destPath.y + "]";
            }
            Debug.Log(debugString);
            Assert.IsTrue(hexPathList[0].IsPathEqualTo(new Vector2Int(2, 2), new Vector2Int(3, 1)));
            Assert.IsTrue(hexPathList[1].IsPathEqualTo(new Vector2Int(3, 1), new Vector2Int(4, 1)));
            Assert.IsTrue(hexPathList[2].IsPathEqualTo(new Vector2Int(4, 1), new Vector2Int(4, 2)));
            Assert.IsTrue(hexPathList[3].IsPathEqualTo(new Vector2Int(4, 2), new Vector2Int(3, 3)));
            Assert.IsTrue(hexPathList[4].IsPathEqualTo(new Vector2Int(3, 3), new Vector2Int(2, 4)));
            Assert.IsTrue(hexPathList[5].IsPathEqualTo(new Vector2Int(2, 4), new Vector2Int(1, 4)));
            Assert.AreEqual(hexPathList.Count, 6);
        }

        private void CreateTheGrid()
        {
            GameObject.Instantiate(TestController.instance.battleControllerPrefab as GameObject);
            GridController.instance.rootObject = new GameObject();
            GridController.instance.listOfMaps = TestController.instance.listOfMaps;
            GridController.instance.map = TestController.instance.testMap;
            GridController.instance.CreateGrid();
        }

        [SetUp]
        public void LoadTestScene()
        {
            if (SceneManager.GetActiveScene().name != "testScene")
            {
                SceneManager.LoadScene("testScene", LoadSceneMode.Single);
            }
        }

        [TearDown]
        public void UnloadTestScene()
        {
            if (SceneManager.GetActiveScene().name != "testScene")
            {
                SceneManager.UnloadSceneAsync("testScene");
            }
        }

        [TearDown]
        public void DestroyAllGameObjects()
        {
            foreach (GameObject go in Object.FindObjectsOfType<GameObject>())
            {
                Object.Destroy(go);
            }
        }
    }
}