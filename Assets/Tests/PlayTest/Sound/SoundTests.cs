﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Tests
{
    public class SoundTests
    {

        [UnityTest]
        public IEnumerator TestDefaultBGMLoaded()
        {
            Debug.Log("This test checks that the default BGM Loaded is the expected one");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            string DefaultBGMLoaded = SoundController.instance.GetCurrentBGMClip();
            Assert.AreEqual(DefaultBGMLoaded, "BgmIntro");
        }

        [UnityTest]
        public IEnumerator TestPlayBGM()
        {
            Debug.Log("This test checks that playing a BGM actually plays it");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.PlayBackgroundMusic(TestController.instance.bgmDungeon, 1, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGMClip(), "BgmDungeon");
        }

        [UnityTest]
        public IEnumerator TestPlayBGMAndCheckVolume()
        {
            Debug.Log("This test checks that playing a BGM plays it at the volume defined");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.PlayBackgroundMusic(TestController.instance.bgmDungeon, 0.4f, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGMAudioSourceVolume(), 0.4f);
        }

        [UnityTest]
        public IEnumerator TestPlay2BGMInARow()
        {
            Debug.Log("This test checks that playing 2 BGM actually plays them. This test is needed because we are using 2 channels to play the BGM");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.PlayBackgroundMusic(TestController.instance.bgmDungeon, 1, 0.01f);
            yield return SoundController.instance.PlayBackgroundMusic(TestController.instance.bgmForest, 1, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGMClip(), "BgmForest");
        }

        [UnityTest]
        public IEnumerator TestPlayBGMAlreadyLoadedNoTransitionTriggered()
        {
            Debug.Log("This test checks that playing a BGM already loaded does not trigger a transition");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            int currentPointer = SoundController.instance.BGMPointer;
            yield return SoundController.instance.PlayBackgroundMusic(TestController.instance.bgmIntro, 1, 0.01f);
            Assert.AreEqual(currentPointer, SoundController.instance.BGMPointer);
        }

        [UnityTest]
        public IEnumerator TestStopBGM()
        {
            Debug.Log("This test checks that after stopping a BGM, no BGM is playing");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.StopBackgroundMusic(0.01f);
            Assert.IsNull(SoundController.instance.GetCurrentBGMClip());
        }

        [UnityTest]
        public IEnumerator TestStopBGMAndPlayAnotherBGM()
        {
            Debug.Log("This test checks that when stopping BGM and then playing another BGM, the new BGM is actually played");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.StopBackgroundMusic(0.01f);
            yield return SoundController.instance.PlayBackgroundMusic(TestController.instance.bgmDungeon, 1, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGMClip(), "BgmDungeon");
        }

        [UnityTest]
        public IEnumerator TestStopBGMAndPlayAgainSameBGM()
        {
            Debug.Log("This test checks that when stopping BGM and then playing it again works");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.StopBackgroundMusic(0.01f);
            yield return SoundController.instance.PlayBackgroundMusic(TestController.instance.bgmForest, 1, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGMClip(), "BgmForest");
        }

        [UnityTest]
        public IEnumerator TestSetVolumeToBGM()
        {
            Debug.Log("This test checks that the function to set the volume to BGM set the audio source to the right volume");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.PlayBackgroundMusic(TestController.instance.bgmDungeon, 0.4f, 0.01f);
            yield return SoundController.instance.SetBGMVolumeTo(0.8f, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGMAudioSourceVolume(), 0.8f);
        }

        [UnityTest]
        public IEnumerator TestDefaultBGSLoaded()
        {
            Debug.Log("This test checks that the default BGS Loaded is the expected one");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            string DefaultBGSLoaded = SoundController.instance.GetCurrentBGSClip();
            Assert.AreEqual(DefaultBGSLoaded, "BgsForest");
        }

        [UnityTest]
        public IEnumerator TestPlayBGS()
        {
            Debug.Log("This test checks that playing a BGS actually plays it");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.PlayBackgroundSound(TestController.instance.bgsDungeon, 1, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGSClip(), "BgsDungeon");
        }

        [UnityTest]
        public IEnumerator TestPlayBGSAndCheckVolume()
        {
            Debug.Log("This test checks that playing a BGS plays it at the volume defined");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.PlayBackgroundSound(TestController.instance.bgmDungeon, 0.4f, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGSAudioSourceVolume(), 0.4f);
        }

        [UnityTest]
        public IEnumerator TestPlay2BGSInARow()
        {
            Debug.Log("This test checks that playing 2 BGS actually plays them. This test is needed because we are using 2 channels to play the BGS");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.PlayBackgroundSound(TestController.instance.bgmDungeon, 1, 0.01f);
            yield return SoundController.instance.PlayBackgroundSound(TestController.instance.bgsForest, 1, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGSClip(), "BgsForest");
        }

        [UnityTest]
        public IEnumerator TestPlayBGSAlreadyLoadedNoTransitionTriggered()
        {
            Debug.Log("This test checks that playing a BGS already loaded does not trigger a transition");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            int currentPointer = SoundController.instance.BGSPointer;
            yield return SoundController.instance.PlayBackgroundSound(TestController.instance.bgsForest, 1, 0.01f);
            Assert.AreEqual(currentPointer, SoundController.instance.BGSPointer);
        }

        [UnityTest]
        public IEnumerator TestStopBGS()
        {
            Debug.Log("This test checks that after stopping a BGS, no BGS is playing");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.StopBackgroundSound(0.01f);
            Assert.IsNull(SoundController.instance.GetCurrentBGSClip());
        }

        [UnityTest]
        public IEnumerator TestStopBGSAndPlayAnotherBGS()
        {
            Debug.Log("This test checks that when stopping BGS and then playing another BGS, the new BGS is actually played");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.StopBackgroundSound(0.01f);
            yield return SoundController.instance.PlayBackgroundSound(TestController.instance.bgsDungeon, 1, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGSClip(), "BgsDungeon");
        }

        [UnityTest]
        public IEnumerator TestStopBGSAndPlayAgainSameBGS()
        {
            Debug.Log("This test checks that when stopping BGS and then playing it again works");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.StopBackgroundSound(0.01f);
            yield return SoundController.instance.PlayBackgroundSound(TestController.instance.bgsForest, 1, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGSClip(), "BgsForest");
        }

        [UnityTest]
        public IEnumerator TestSetVolumeToBGS()
        {
            Debug.Log("This test checks that the function to set the volume to BGS set the audio source to the right volume");
            GameObject.Instantiate(TestController.instance.soundController as GameObject);
            yield return null;
            yield return SoundController.instance.PlayBackgroundSound(TestController.instance.bgsDungeon, 0.4f, 0.01f);
            yield return SoundController.instance.SetBGSVolumeTo(0.8f, 0.01f);
            Assert.AreEqual(SoundController.instance.GetCurrentBGSAudioSourceVolume(), 0.8f);
        }

        [SetUp]
        public void LoadTestScene()
        {
            if (SceneManager.GetActiveScene().name != "testScene")
            {
                SceneManager.LoadScene("testScene", LoadSceneMode.Single);
            }
        }

        [TearDown]
        public void UnloadTestScene()
        {
            if (SceneManager.GetActiveScene().name != "testScene")
            {
                SceneManager.UnloadSceneAsync("testScene");
            }
        }

        [TearDown]
        public void DestroyAllGameObjects()
        {
            foreach (GameObject go in Object.FindObjectsOfType<GameObject>())
            {
                Object.Destroy(go);
            }
        }
    }
}