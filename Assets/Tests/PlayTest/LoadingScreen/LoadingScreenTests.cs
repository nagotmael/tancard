﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

namespace Tests
{
    public class LoadingScreenTests
    {
        [UnityTest]
        public IEnumerator TestSceneIsLoadedWhileLoadingScreenIsDone()
        {
            Debug.Log("This test checks that when loading a scene using the loading screen function, the scene is active at the end");
            yield return null;
            yield return loadLocalization();
            GameObject.Instantiate(TestController.instance.loadingScreenController as GameObject);
            yield return null;
            yield return LoadingScreenController.instance.LoadScene("Logo", true);
            Assert.IsNotNull(GameObject.Find("Logo"));
            SceneManager.SetActiveScene(SceneManager.GetSceneByName("testScene"));
            SceneManager.UnloadSceneAsync("Logo");
        }

        [SetUp]
        public void LoadTestScene()
        {
            if (SceneManager.GetActiveScene().name != "testScene")
            {
                SceneManager.LoadScene("testScene", LoadSceneMode.Single);
            }
        }

        [TearDown]
        public void UnloadTestScene()
        {
            if (SceneManager.GetActiveScene().name != "testScene")
            {
                SceneManager.UnloadSceneAsync("testScene");
            }
        }

        [TearDown]
        public void DestroyAllGameObjects()
        {
            foreach (GameObject go in Object.FindObjectsOfType<GameObject>())
            {
                Object.Destroy(go);
            }
        }

        public IEnumerator loadLocalization()
        {
            GameObject.Instantiate(TestController.instance.localizationController as GameObject);
            yield return null;
            LocalizationController.instance.LoadLocalizedText("localizedText_en.json");

            while (!LocalizationController.instance.GetIsReady())
            {
                yield return null;
            }
        }

    }
}
